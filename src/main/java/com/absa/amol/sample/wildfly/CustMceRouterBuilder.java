package com.absa.amol.sample.wildfly;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.common.message.CxfConstants;

import com.barclays.mce.scv.scv.SCVService;

@ApplicationScoped
public class CustMceRouterBuilder extends RouteBuilder {
	//private static final String CXF_RS_ENDPOINT_URI = "cxfrs://http://localhost:" + CXT + "/rest?resourceClasses=org.apache.camel.component.cxf.jaxrs.testbean.CustomerServiceResource";

	@Override
	public void configure() throws Exception {
				
		from("direct:SearchCustomerBySCVID")       
		
		//.setBody(constant("100000000465"))
		.bean(SearchCustBySCVIDReqBuilder.class,"buildSoapReq")
		.setHeader(CxfConstants.OPERATION_NAME, constant("SearchCustomerBySCVID"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/"))
		/*.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SearchCustByNameReq"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVError"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SearchCustResults"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SearchCustBySCVIDReq"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SearchCustByPPIdReq"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SearchCustByIdentifiersReq"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVBaseDataTypes"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVBaseRequest"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVServiceHeader"))
		
		
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVErrorList"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVDetails"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SCVBaseResponse"))
		.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://scv.mce.barclays.com/scv/SearchCustomerBySCVID"))*/
      		
		.to("cxf://http://zadsdcrapp1311.corp.dsarena.com:9080/MCEServiceWS/SCVService?serviceClass="
				+ SCVService.class.getName()
				+"&wsdlURL=/wsdl/SCVService.wsdl")
		//.bean(SearchCustBySCVIDReqBuilder.class,"parseSoapResp")
		.process(new CustInfoSystemMceProcess());
		//.to("log:output");			 
       // .log("*********** success *********** : ${body[0]}");
		 
		
	}


}
