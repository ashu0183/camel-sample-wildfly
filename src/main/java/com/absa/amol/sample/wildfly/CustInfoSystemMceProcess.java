package com.absa.amol.sample.wildfly;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.barclays.mce.scv.scv.scvdetails.SCVDetails;
import com.barclays.mce.scv.scv.searchcustresults.SearchCustResults;

public class CustInfoSystemMceProcess implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		String output="";
		
			SearchCustResults res = exchange.getIn().getBody(SearchCustResults.class);     
			System.out.println("res=======> : " + res+"===>record count==>"+res.getTotalRecordCount());
			
			List<SCVDetails> result = res.getSCVDetails();
			for (SCVDetails data : result) {
				output = data.getFirstName();
				System.out.println("output=======> : " + output);
				/*jsonObject.put("FirstName", data.getFirstName());
				jsonObject.put("LastName", data.getLastName());
				jsonObject.put("CustomerFullName", data.getCustomerFullName());
				jsonObject.put("MobileNumber", data.getMobileNumber());
				jsonObject.put("DateOfBirth", data.getDateOfBirth());*/
			}
			
			/*
			 * } catch (Exception e) { System.out.println("Exception in process : " + e); }
			 */
		exchange.getIn().setBody(output);
	}
}