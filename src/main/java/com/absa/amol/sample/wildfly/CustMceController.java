package com.absa.amol.sample.wildfly;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.apache.camel.ProducerTemplate;


@Path("/customerBasic")
@ApplicationScoped
public class CustMceController {
	
	@Inject   
	ProducerTemplate producerTemplate;		        
	
    @GET
    public String retrieveCustomerBasicInfo() {
    	String name="test camel fuse library";
    	//ProducerTemplate producerTemplate = camelctx.createProducerTemplate();
    	String result = producerTemplate.requestBody("direct:SearchCustomerBySCVID", name, String.class);
    	
		
        return result;
    }



   
}
