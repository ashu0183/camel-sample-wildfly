package com.absa.amol.sample.wildfly;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;

@ApplicationScoped
//@Startup
//@ContextName("cdi-context")
public class MyRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
    	//from("direct:start").bean("helloBean");
		from("direct:start").process(new Processor() {
			public void process(Exchange xchg) throws Exception {
				String name = xchg.getIn().getBody(String.class);
				xchg.getIn().setBody(name);
			}
		});
		
		
    	
    }
}
