package com.absa.amol.sample.wildfly;

import java.util.List;

import com.barclays.mce.scv.scv.scvdetails.SCVDetails;
import com.barclays.mce.scv.scv.scvserviceheader.SCVReqHeader;
import com.barclays.mce.scv.scv.searchcustbyscvidreq.SearchCustBySCVIDReq;
import com.barclays.mce.scv.scv.searchcustresults.SearchCustResults;

public class SearchCustBySCVIDReqBuilder {

	
	/*public SearchCustBySCVIDReq SearchCustomerBySCVID(String scvId) {

		SearchCustBySCVIDReq request = new SearchCustBySCVIDReq();
		SCVReqHeader scvReqHeader = new SCVReqHeader();
		scvReqHeader.setBusinessID("BWBRB");
		scvReqHeader.setOriginatingChannel("UB");
		scvReqHeader.setUserID("IFE");
		scvReqHeader.setServiceDateTime("2020-05-28T11:17:58.231Z");
		
		request.setSCVID(scvId);
		request.setRequestHeader(scvReqHeader);
		return request;
	}
	*/
	public SearchCustBySCVIDReq buildSoapReq(String scvId) {

		SearchCustBySCVIDReq request = new SearchCustBySCVIDReq();
		SCVReqHeader scvReqHeader = new SCVReqHeader();
		scvReqHeader.setBusinessID("BWBRB");
		scvReqHeader.setOriginatingChannel("UB");
		scvReqHeader.setUserID("IFE");
		scvReqHeader.setServiceDateTime("2020-05-28T11:17:58.231Z");
		
		request.setSCVID("100000000465");
		request.setRequestHeader(scvReqHeader);
		return request;
	}
	
	public String parseSoapResp(SearchCustResults SearchCustResults) {
		String output="";
		List<SCVDetails> result = SearchCustResults.getSCVDetails();
		for (SCVDetails data : result) {
			output = data.getFirstName();
			System.out.println("output=======> : " + output);
			/*jsonObject.put("FirstName", data.getFirstName());
			jsonObject.put("LastName", data.getLastName());
			jsonObject.put("CustomerFullName", data.getCustomerFullName());
			jsonObject.put("MobileNumber", data.getMobileNumber());
			jsonObject.put("DateOfBirth", data.getDateOfBirth());*/
		}
		return output;
	}
}
	
