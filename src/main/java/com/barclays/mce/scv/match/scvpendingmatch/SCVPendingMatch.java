
package com.barclays.mce.scv.match.scvpendingmatch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.grcb.mce.userauditdetails.UserAuditDetails;
import com.barclays.mce.scv.match.scvmatch.SCVMatch;


/**
 * <p>Java class for SCVPendingMatch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVPendingMatch"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/match/SCVMatch}SCVMatch"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkID" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="UserAuditDetails" type="{http://grcb.barclays.com/mce/UserAuditDetails}UserAuditDetails" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVPendingMatch", propOrder = {
    "linkID",
    "userAuditDetails"
})
public class SCVPendingMatch
    extends SCVMatch
{

    @XmlElement(name = "LinkID", required = true)
    protected String linkID;
    @XmlElement(name = "UserAuditDetails")
    protected UserAuditDetails userAuditDetails;

    /**
     * Gets the value of the linkID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkID() {
        return linkID;
    }

    /**
     * Sets the value of the linkID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkID(String value) {
        this.linkID = value;
    }

    /**
     * Gets the value of the userAuditDetails property.
     * 
     * @return
     *     possible object is
     *     {@link UserAuditDetails }
     *     
     */
    public UserAuditDetails getUserAuditDetails() {
        return userAuditDetails;
    }

    /**
     * Sets the value of the userAuditDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserAuditDetails }
     *     
     */
    public void setUserAuditDetails(UserAuditDetails value) {
        this.userAuditDetails = value;
    }

}
