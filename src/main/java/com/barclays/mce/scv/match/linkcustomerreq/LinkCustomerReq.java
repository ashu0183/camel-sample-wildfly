
package com.barclays.mce.scv.match.linkcustomerreq;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.scvmatch.LinkPair;
import com.barclays.mce.scv.scv.scvbaserequest.SCVBaseRequest;


/**
 * <p>Java class for LinkCustomerReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkCustomerReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseRequest}SCVBaseRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkPair" type="{http://scv.mce.barclays.com/match/SCVMatch}LinkPair" maxOccurs="unbounded" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkCustomerReq", propOrder = {
    "linkPair"
})
public class LinkCustomerReq
    extends SCVBaseRequest
{

    @XmlElement(name = "LinkPair", required = true)
    protected List<LinkPair> linkPair;

    /**
     * Gets the value of the linkPair property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkPair property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkPair().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LinkPair }
     * 
     * 
     */
    public List<LinkPair> getLinkPair() {
        if (linkPair == null) {
            linkPair = new ArrayList<LinkPair>();
        }
        return this.linkPair;
    }

}
