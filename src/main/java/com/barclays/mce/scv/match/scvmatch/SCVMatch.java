
package com.barclays.mce.scv.match.scvmatch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.bankcustomer.BANKCustomer;
import com.barclays.mce.scv.match.cardcustomer.CARDCustomer;
import com.barclays.mce.scv.match.scvpendingmatch.SCVPendingMatch;


/**
 * <p>Java class for SCVMatch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVMatch"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BankCustomer" type="{http://scv.mce.barclays.com/match/BANKCustomer}BANKCustomer" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CARDCustomer" type="{http://scv.mce.barclays.com/match/CARDCustomer}CARDCustomer" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVMatch", propOrder = {
    "bankCustomer",
    "cardCustomer"
})
@XmlSeeAlso({
    SCVPendingMatch.class
})
public class SCVMatch {

    @XmlElement(name = "BankCustomer")
    protected BANKCustomer bankCustomer;
    @XmlElement(name = "CARDCustomer")
    protected CARDCustomer cardCustomer;

    /**
     * Gets the value of the bankCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link BANKCustomer }
     *     
     */
    public BANKCustomer getBankCustomer() {
        return bankCustomer;
    }

    /**
     * Sets the value of the bankCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BANKCustomer }
     *     
     */
    public void setBankCustomer(BANKCustomer value) {
        this.bankCustomer = value;
    }

    /**
     * Gets the value of the cardCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link CARDCustomer }
     *     
     */
    public CARDCustomer getCARDCustomer() {
        return cardCustomer;
    }

    /**
     * Sets the value of the cardCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CARDCustomer }
     *     
     */
    public void setCARDCustomer(CARDCustomer value) {
        this.cardCustomer = value;
    }

}
