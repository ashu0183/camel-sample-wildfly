
package com.barclays.mce.scv.match.cardcustomer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.customer.Customer;


/**
 * <p>Java class for CARDCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CARDCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/match/Customer}Customer"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CardCIF" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CARDCustomer", propOrder = {
    "cardCIF"
})
public class CARDCustomer
    extends Customer
{

    @XmlElement(name = "CardCIF", required = true)
    protected String cardCIF;

    /**
     * Gets the value of the cardCIF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCIF() {
        return cardCIF;
    }

    /**
     * Sets the value of the cardCIF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCIF(String value) {
        this.cardCIF = value;
    }

}
