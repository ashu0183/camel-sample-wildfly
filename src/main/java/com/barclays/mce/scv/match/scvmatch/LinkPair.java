
package com.barclays.mce.scv.match.scvmatch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LinkPair complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkPair"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BusinessID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="BankCIF" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="CardCIF" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="MarkForDelete" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkPair", propOrder = {
    "businessID",
    "bankCIF",
    "cardCIF",
    "markForDelete"
})
public class LinkPair {

    @XmlElement(name = "BusinessID")
    protected String businessID;
    @XmlElement(name = "BankCIF", required = true)
    protected String bankCIF;
    @XmlElement(name = "CardCIF", required = true)
    protected String cardCIF;
    @XmlElement(name = "MarkForDelete")
    protected String markForDelete;

    /**
     * Gets the value of the businessID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessID() {
        return businessID;
    }

    /**
     * Sets the value of the businessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessID(String value) {
        this.businessID = value;
    }

    /**
     * Gets the value of the bankCIF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCIF() {
        return bankCIF;
    }

    /**
     * Sets the value of the bankCIF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCIF(String value) {
        this.bankCIF = value;
    }

    /**
     * Gets the value of the cardCIF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCIF() {
        return cardCIF;
    }

    /**
     * Sets the value of the cardCIF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCIF(String value) {
        this.cardCIF = value;
    }

    /**
     * Gets the value of the markForDelete property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarkForDelete() {
        return markForDelete;
    }

    /**
     * Sets the value of the markForDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarkForDelete(String value) {
        this.markForDelete = value;
    }

}
