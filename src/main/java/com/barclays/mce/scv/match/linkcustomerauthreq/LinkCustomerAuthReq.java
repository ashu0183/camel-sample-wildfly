
package com.barclays.mce.scv.match.linkcustomerauthreq;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaserequest.SCVBaseRequest;


/**
 * <p>Java class for LinkCustomerAuthReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkCustomerAuthReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseRequest}SCVBaseRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AuthResult" type="{http://www.w3.org/2001/XMLSchema}boolean" form="qualified"/&gt;
 *         &lt;element name="BusinessID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="LinkIDs" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkCustomerAuthReq", propOrder = {
    "authResult",
    "businessID",
    "linkIDs"
})
public class LinkCustomerAuthReq
    extends SCVBaseRequest
{

    @XmlElement(name = "AuthResult")
    protected boolean authResult;
    @XmlElement(name = "BusinessID")
    protected String businessID;
    @XmlElement(name = "LinkIDs", required = true)
    protected List<String> linkIDs;

    /**
     * Gets the value of the authResult property.
     * 
     */
    public boolean isAuthResult() {
        return authResult;
    }

    /**
     * Sets the value of the authResult property.
     * 
     */
    public void setAuthResult(boolean value) {
        this.authResult = value;
    }

    /**
     * Gets the value of the businessID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessID() {
        return businessID;
    }

    /**
     * Sets the value of the businessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessID(String value) {
        this.businessID = value;
    }

    /**
     * Gets the value of the linkIDs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkIDs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkIDs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLinkIDs() {
        if (linkIDs == null) {
            linkIDs = new ArrayList<String>();
        }
        return this.linkIDs;
    }

}
