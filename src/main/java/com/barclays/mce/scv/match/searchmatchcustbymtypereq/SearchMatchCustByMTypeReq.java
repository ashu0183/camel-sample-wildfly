
package com.barclays.mce.scv.match.searchmatchcustbymtypereq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaserequest.SCVBaseRequest;


/**
 * <p>Java class for SearchMatchCustByMTypeReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMatchCustByMTypeReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseRequest}SCVBaseRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BusinessID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}boolean" form="qualified"/&gt;
 *         &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}boolean" form="qualified"/&gt;
 *         &lt;element name="Mobile" type="{http://www.w3.org/2001/XMLSchema}boolean" form="qualified"/&gt;
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *         &lt;element name="NoOfRecordsPerPage" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMatchCustByMTypeReq", propOrder = {
    "businessID",
    "id",
    "dob",
    "mobile",
    "pageNumber",
    "noOfRecordsPerPage"
})
public class SearchMatchCustByMTypeReq
    extends SCVBaseRequest
{

    @XmlElement(name = "BusinessID")
    protected String businessID;
    @XmlElement(name = "ID")
    protected boolean id;
    @XmlElement(name = "DOB")
    protected boolean dob;
    @XmlElement(name = "Mobile")
    protected boolean mobile;
    @XmlElement(name = "PageNumber")
    protected int pageNumber;
    @XmlElement(name = "NoOfRecordsPerPage")
    protected int noOfRecordsPerPage;

    /**
     * Gets the value of the businessID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessID() {
        return businessID;
    }

    /**
     * Sets the value of the businessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessID(String value) {
        this.businessID = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public boolean isID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(boolean value) {
        this.id = value;
    }

    /**
     * Gets the value of the dob property.
     * 
     */
    public boolean isDOB() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     * 
     */
    public void setDOB(boolean value) {
        this.dob = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     */
    public boolean isMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     */
    public void setMobile(boolean value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the noOfRecordsPerPage property.
     * 
     */
    public int getNoOfRecordsPerPage() {
        return noOfRecordsPerPage;
    }

    /**
     * Sets the value of the noOfRecordsPerPage property.
     * 
     */
    public void setNoOfRecordsPerPage(int value) {
        this.noOfRecordsPerPage = value;
    }

}
