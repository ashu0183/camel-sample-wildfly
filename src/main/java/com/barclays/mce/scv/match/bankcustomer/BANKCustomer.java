
package com.barclays.mce.scv.match.bankcustomer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.customer.Customer;


/**
 * <p>Java class for BANKCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BANKCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/match/Customer}Customer"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BankCIF" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BANKCustomer", propOrder = {
    "bankCIF"
})
public class BANKCustomer
    extends Customer
{

    @XmlElement(name = "BankCIF", required = true)
    protected String bankCIF;

    /**
     * Gets the value of the bankCIF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCIF() {
        return bankCIF;
    }

    /**
     * Sets the value of the bankCIF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCIF(String value) {
        this.bankCIF = value;
    }

}
