
package com.barclays.mce.scv.match.searchpendingmatchresult;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.scvpendingmatch.SCVPendingMatch;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for SearchPendingMatchResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchPendingMatchResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TotalRecordCount" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *         &lt;element name="SCVPendingMatch" type="{http://scv.mce.barclays.com/match/SCVPendingMatch}SCVPendingMatch" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchPendingMatchResult", propOrder = {
    "totalRecordCount",
    "scvPendingMatch"
})
public class SearchPendingMatchResult
    extends SCVBaseResponse
{

    @XmlElement(name = "TotalRecordCount")
    protected int totalRecordCount;
    @XmlElement(name = "SCVPendingMatch")
    protected List<SCVPendingMatch> scvPendingMatch;

    /**
     * Gets the value of the totalRecordCount property.
     * 
     */
    public int getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * Sets the value of the totalRecordCount property.
     * 
     */
    public void setTotalRecordCount(int value) {
        this.totalRecordCount = value;
    }

    /**
     * Gets the value of the scvPendingMatch property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scvPendingMatch property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCVPendingMatch().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCVPendingMatch }
     * 
     * 
     */
    public List<SCVPendingMatch> getSCVPendingMatch() {
        if (scvPendingMatch == null) {
            scvPendingMatch = new ArrayList<SCVPendingMatch>();
        }
        return this.scvPendingMatch;
    }

}
