
package com.barclays.mce.scv.match.customerpair;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.bankcustomer.BANKCustomer;
import com.barclays.mce.scv.match.cardcustomer.CARDCustomer;


/**
 * <p>Java class for CustomerPair complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerPair"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BankCustomer" type="{http://scv.mce.barclays.com/match/BANKCustomer}BANKCustomer" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CardCustomer" type="{http://scv.mce.barclays.com/match/CARDCustomer}CARDCustomer" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerPair", propOrder = {
    "bankCustomer",
    "cardCustomer"
})
public class CustomerPair {

    @XmlElement(name = "BankCustomer")
    protected BANKCustomer bankCustomer;
    @XmlElement(name = "CardCustomer")
    protected CARDCustomer cardCustomer;

    /**
     * Gets the value of the bankCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link BANKCustomer }
     *     
     */
    public BANKCustomer getBankCustomer() {
        return bankCustomer;
    }

    /**
     * Sets the value of the bankCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BANKCustomer }
     *     
     */
    public void setBankCustomer(BANKCustomer value) {
        this.bankCustomer = value;
    }

    /**
     * Gets the value of the cardCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link CARDCustomer }
     *     
     */
    public CARDCustomer getCardCustomer() {
        return cardCustomer;
    }

    /**
     * Sets the value of the cardCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CARDCustomer }
     *     
     */
    public void setCardCustomer(CARDCustomer value) {
        this.cardCustomer = value;
    }

}
