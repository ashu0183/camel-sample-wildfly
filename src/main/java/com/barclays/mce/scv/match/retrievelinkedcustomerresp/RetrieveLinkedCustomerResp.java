
package com.barclays.mce.scv.match.retrievelinkedcustomerresp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.customerpair.CustomerPair;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for RetrieveLinkedCustomerResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveLinkedCustomerResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomerPairs" type="{http://scv.mce.barclays.com/match/CustomerPair}CustomerPair" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveLinkedCustomerResp", propOrder = {
    "customerPairs"
})
public class RetrieveLinkedCustomerResp
    extends SCVBaseResponse
{

    @XmlElement(name = "CustomerPairs")
    protected List<CustomerPair> customerPairs;

    /**
     * Gets the value of the customerPairs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerPairs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerPairs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerPair }
     * 
     * 
     */
    public List<CustomerPair> getCustomerPairs() {
        if (customerPairs == null) {
            customerPairs = new ArrayList<CustomerPair>();
        }
        return this.customerPairs;
    }

}
