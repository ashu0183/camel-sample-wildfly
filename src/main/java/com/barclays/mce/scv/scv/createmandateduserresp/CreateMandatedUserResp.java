
package com.barclays.mce.scv.scv.createmandateduserresp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for CreateMandatedUserResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateMandatedUserResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MandatedUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateMandatedUserResp", propOrder = {
    "mandatedUserId"
})
public class CreateMandatedUserResp
    extends SCVBaseResponse
{

    @XmlElement(name = "MandatedUserId")
    protected String mandatedUserId;

    /**
     * Gets the value of the mandatedUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatedUserId() {
        return mandatedUserId;
    }

    /**
     * Sets the value of the mandatedUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatedUserId(String value) {
        this.mandatedUserId = value;
    }

}
