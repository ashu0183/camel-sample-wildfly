
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for UnlinkCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnlinkCustomerResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnlinkCustomerResponse" type="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnlinkCustomerResponse", propOrder = {
    "unlinkCustomerResponse"
})
public class UnlinkCustomerResponse {

    @XmlElement(name = "UnlinkCustomerResponse")
    protected SCVBaseResponse unlinkCustomerResponse;

    /**
     * Gets the value of the unlinkCustomerResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SCVBaseResponse }
     *     
     */
    public SCVBaseResponse getUnlinkCustomerResponse() {
        return unlinkCustomerResponse;
    }

    /**
     * Sets the value of the unlinkCustomerResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVBaseResponse }
     *     
     */
    public void setUnlinkCustomerResponse(SCVBaseResponse value) {
        this.unlinkCustomerResponse = value;
    }

}
