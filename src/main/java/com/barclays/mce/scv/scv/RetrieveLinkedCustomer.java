
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.retrievelinkedcustomerreq.RetrieveLinkedCustomerReq;


/**
 * <p>Java class for RetrieveLinkedCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveLinkedCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetrieveLinkedCustomerReq" type="{http://scv.mce.barclays.com/match/RetrieveLinkedCustomerReq}RetrieveLinkedCustomerReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveLinkedCustomer", propOrder = {
    "retrieveLinkedCustomerReq"
})
public class RetrieveLinkedCustomer {

    @XmlElement(name = "RetrieveLinkedCustomerReq")
    protected RetrieveLinkedCustomerReq retrieveLinkedCustomerReq;

    /**
     * Gets the value of the retrieveLinkedCustomerReq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveLinkedCustomerReq }
     *     
     */
    public RetrieveLinkedCustomerReq getRetrieveLinkedCustomerReq() {
        return retrieveLinkedCustomerReq;
    }

    /**
     * Sets the value of the retrieveLinkedCustomerReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveLinkedCustomerReq }
     *     
     */
    public void setRetrieveLinkedCustomerReq(RetrieveLinkedCustomerReq value) {
        this.retrieveLinkedCustomerReq = value;
    }

}
