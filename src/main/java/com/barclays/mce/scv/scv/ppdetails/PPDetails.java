
package com.barclays.mce.scv.scv.ppdetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PPDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PPDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProductProcessorType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="ProductProcessorId" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PPDetails", propOrder = {
    "productProcessorType",
    "productProcessorId"
})
public class PPDetails {

    @XmlElement(name = "ProductProcessorType", required = true)
    protected String productProcessorType;
    @XmlElement(name = "ProductProcessorId", required = true)
    protected String productProcessorId;

    /**
     * Gets the value of the productProcessorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductProcessorType() {
        return productProcessorType;
    }

    /**
     * Sets the value of the productProcessorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductProcessorType(String value) {
        this.productProcessorType = value;
    }

    /**
     * Gets the value of the productProcessorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductProcessorId() {
        return productProcessorId;
    }

    /**
     * Sets the value of the productProcessorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductProcessorId(String value) {
        this.productProcessorId = value;
    }

}
