
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchnonindividualcustomerreq.SearchNonIndividualCustomerReq;


/**
 * <p>Java class for SearchNonIndividualCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchNonIndividualCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchNonIndividualCustomerReq" type="{http://scv.mce.barclays.com/scv/SearchNonIndividualCustomerReq}SearchNonIndividualCustomerReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchNonIndividualCustomer", propOrder = {
    "searchNonIndividualCustomerReq"
})
public class SearchNonIndividualCustomer {

    @XmlElement(name = "SearchNonIndividualCustomerReq")
    protected SearchNonIndividualCustomerReq searchNonIndividualCustomerReq;

    /**
     * Gets the value of the searchNonIndividualCustomerReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchNonIndividualCustomerReq }
     *     
     */
    public SearchNonIndividualCustomerReq getSearchNonIndividualCustomerReq() {
        return searchNonIndividualCustomerReq;
    }

    /**
     * Sets the value of the searchNonIndividualCustomerReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchNonIndividualCustomerReq }
     *     
     */
    public void setSearchNonIndividualCustomerReq(SearchNonIndividualCustomerReq value) {
        this.searchNonIndividualCustomerReq = value;
    }

}
