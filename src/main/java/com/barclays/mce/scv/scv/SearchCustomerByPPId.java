
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchcustbyppidreq.SearchCustByPPIdReq;


/**
 * <p>Java class for SearchCustomerByPPId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustomerByPPId"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustByPPIdReq" type="{http://scv.mce.barclays.com/scv/SearchCustByPPIdReq}SearchCustByPPIdReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustomerByPPId", propOrder = {
    "searchCustByPPIdReq"
})
public class SearchCustomerByPPId {

    @XmlElement(name = "SearchCustByPPIdReq")
    protected SearchCustByPPIdReq searchCustByPPIdReq;

    /**
     * Gets the value of the searchCustByPPIdReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCustByPPIdReq }
     *     
     */
    public SearchCustByPPIdReq getSearchCustByPPIdReq() {
        return searchCustByPPIdReq;
    }

    /**
     * Sets the value of the searchCustByPPIdReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCustByPPIdReq }
     *     
     */
    public void setSearchCustByPPIdReq(SearchCustByPPIdReq value) {
        this.searchCustByPPIdReq = value;
    }

}
