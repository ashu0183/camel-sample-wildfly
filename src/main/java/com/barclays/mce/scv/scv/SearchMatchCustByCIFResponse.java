
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.searchmatchresult.SearchMatchResult;


/**
 * <p>Java class for SearchMatchCustByCIFResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMatchCustByCIFResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchMatchResult" type="{http://scv.mce.barclays.com/match/SearchMatchResult}SearchMatchResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMatchCustByCIFResponse", propOrder = {
    "searchMatchResult"
})
public class SearchMatchCustByCIFResponse {

    @XmlElement(name = "SearchMatchResult")
    protected SearchMatchResult searchMatchResult;

    /**
     * Gets the value of the searchMatchResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchMatchResult }
     *     
     */
    public SearchMatchResult getSearchMatchResult() {
        return searchMatchResult;
    }

    /**
     * Sets the value of the searchMatchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMatchResult }
     *     
     */
    public void setSearchMatchResult(SearchMatchResult value) {
        this.searchMatchResult = value;
    }

}
