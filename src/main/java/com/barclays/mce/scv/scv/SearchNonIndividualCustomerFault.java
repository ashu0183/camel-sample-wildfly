
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverror.SCVError;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchNonIndividualCustomer_fault" type="{http://scv.mce.barclays.com/scv/SCVError}SCVError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchNonIndividualCustomerFault"
})
@XmlRootElement(name = "SearchNonIndividualCustomer_fault")
public class SearchNonIndividualCustomerFault {

    @XmlElement(name = "SearchNonIndividualCustomer_fault", required = true)
    protected SCVError searchNonIndividualCustomerFault;

    /**
     * Gets the value of the searchNonIndividualCustomerFault property.
     * 
     * @return
     *     possible object is
     *     {@link SCVError }
     *     
     */
    public SCVError getSearchNonIndividualCustomerFault() {
        return searchNonIndividualCustomerFault;
    }

    /**
     * Sets the value of the searchNonIndividualCustomerFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVError }
     *     
     */
    public void setSearchNonIndividualCustomerFault(SCVError value) {
        this.searchNonIndividualCustomerFault = value;
    }

}
