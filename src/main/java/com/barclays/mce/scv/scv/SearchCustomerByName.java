
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchcustbynamereq.SearchCustByNameReq;


/**
 * <p>Java class for SearchCustomerByName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustomerByName"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustByNameReq" type="{http://scv.mce.barclays.com/scv/SearchCustByNameReq}SearchCustByNameReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustomerByName", propOrder = {
    "searchCustByNameReq"
})
public class SearchCustomerByName {

    @XmlElement(name = "SearchCustByNameReq")
    protected SearchCustByNameReq searchCustByNameReq;

    /**
     * Gets the value of the searchCustByNameReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCustByNameReq }
     *     
     */
    public SearchCustByNameReq getSearchCustByNameReq() {
        return searchCustByNameReq;
    }

    /**
     * Sets the value of the searchCustByNameReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCustByNameReq }
     *     
     */
    public void setSearchCustByNameReq(SearchCustByNameReq value) {
        this.searchCustByNameReq = value;
    }

}
