
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.createmandateduserreq.CreateMandatedUserReq;


/**
 * <p>Java class for CreateMandatedUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateMandatedUser"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateMandatedUserReq" type="{http://scv.mce.barclays.com/scv/CreateMandatedUserReq}CreateMandatedUserReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateMandatedUser", propOrder = {
    "createMandatedUserReq"
})
public class CreateMandatedUser {

    @XmlElement(name = "CreateMandatedUserReq")
    protected CreateMandatedUserReq createMandatedUserReq;

    /**
     * Gets the value of the createMandatedUserReq property.
     * 
     * @return
     *     possible object is
     *     {@link CreateMandatedUserReq }
     *     
     */
    public CreateMandatedUserReq getCreateMandatedUserReq() {
        return createMandatedUserReq;
    }

    /**
     * Sets the value of the createMandatedUserReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateMandatedUserReq }
     *     
     */
    public void setCreateMandatedUserReq(CreateMandatedUserReq value) {
        this.createMandatedUserReq = value;
    }

}
