
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverror.SCVError;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateMandatedUserStatus_fault" type="{http://scv.mce.barclays.com/scv/SCVError}SCVError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateMandatedUserStatusFault"
})
@XmlRootElement(name = "UpdateMandatedUserStatus_fault")
public class UpdateMandatedUserStatusFault {

    @XmlElement(name = "UpdateMandatedUserStatus_fault", required = true)
    protected SCVError updateMandatedUserStatusFault;

    /**
     * Gets the value of the updateMandatedUserStatusFault property.
     * 
     * @return
     *     possible object is
     *     {@link SCVError }
     *     
     */
    public SCVError getUpdateMandatedUserStatusFault() {
        return updateMandatedUserStatusFault;
    }

    /**
     * Sets the value of the updateMandatedUserStatusFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVError }
     *     
     */
    public void setUpdateMandatedUserStatusFault(SCVError value) {
        this.updateMandatedUserStatusFault = value;
    }

}
