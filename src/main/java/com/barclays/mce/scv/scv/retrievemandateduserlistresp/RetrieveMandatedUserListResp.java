
package com.barclays.mce.scv.scv.retrievemandateduserlistresp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.mandateduser.MandatedUser;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for RetrieveMandatedUserListResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveMandatedUserListResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchMandatedUserResults" type="{http://scv.mce.barclays.com/scv/MandatedUser}MandatedUser" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveMandatedUserListResp", propOrder = {
    "searchMandatedUserResults"
})
public class RetrieveMandatedUserListResp
    extends SCVBaseResponse
{

    @XmlElement(name = "SearchMandatedUserResults")
    protected List<MandatedUser> searchMandatedUserResults;

    /**
     * Gets the value of the searchMandatedUserResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the searchMandatedUserResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchMandatedUserResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MandatedUser }
     * 
     * 
     */
    public List<MandatedUser> getSearchMandatedUserResults() {
        if (searchMandatedUserResults == null) {
            searchMandatedUserResults = new ArrayList<MandatedUser>();
        }
        return this.searchMandatedUserResults;
    }

}
