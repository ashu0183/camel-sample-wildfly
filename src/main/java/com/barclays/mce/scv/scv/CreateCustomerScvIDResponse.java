
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.createcustomerscvidresp.CreateCustomerScvIDResp;


/**
 * <p>Java class for CreateCustomerScvIDResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCustomerScvIDResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateCustomerScvIDResponse" type="{http://scv.mce.barclays.com/scv/CreateCustomerScvIDResp}CreateCustomerScvIDResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCustomerScvIDResponse", propOrder = {
    "createCustomerScvIDResponse"
})
public class CreateCustomerScvIDResponse {

    @XmlElement(name = "CreateCustomerScvIDResponse")
    protected CreateCustomerScvIDResp createCustomerScvIDResponse;

    /**
     * Gets the value of the createCustomerScvIDResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CreateCustomerScvIDResp }
     *     
     */
    public CreateCustomerScvIDResp getCreateCustomerScvIDResponse() {
        return createCustomerScvIDResponse;
    }

    /**
     * Sets the value of the createCustomerScvIDResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateCustomerScvIDResp }
     *     
     */
    public void setCreateCustomerScvIDResponse(CreateCustomerScvIDResp value) {
        this.createCustomerScvIDResponse = value;
    }

}
