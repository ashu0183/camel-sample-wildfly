
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.updatemandateduserreq.UpdateMandatedUserReq;


/**
 * <p>Java class for UpdateMandatedUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateMandatedUser"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateMandatedUserReq" type="{http://scv.mce.barclays.com/scv/UpdateMandatedUserReq}UpdateMandatedUserReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateMandatedUser", propOrder = {
    "updateMandatedUserReq"
})
public class UpdateMandatedUser {

    @XmlElement(name = "UpdateMandatedUserReq")
    protected UpdateMandatedUserReq updateMandatedUserReq;

    /**
     * Gets the value of the updateMandatedUserReq property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateMandatedUserReq }
     *     
     */
    public UpdateMandatedUserReq getUpdateMandatedUserReq() {
        return updateMandatedUserReq;
    }

    /**
     * Sets the value of the updateMandatedUserReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateMandatedUserReq }
     *     
     */
    public void setUpdateMandatedUserReq(UpdateMandatedUserReq value) {
        this.updateMandatedUserReq = value;
    }

}
