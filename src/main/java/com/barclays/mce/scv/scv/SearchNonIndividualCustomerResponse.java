
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchnonindividualcustomerresp.SearchNonIndividualCustomerResp;


/**
 * <p>Java class for SearchNonIndividualCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchNonIndividualCustomerResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchNonIndividualCustomerResp" type="{http://scv.mce.barclays.com/scv/SearchNonIndividualCustomerResp}SearchNonIndividualCustomerResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchNonIndividualCustomerResponse", propOrder = {
    "searchNonIndividualCustomerResp"
})
public class SearchNonIndividualCustomerResponse {

    @XmlElement(name = "SearchNonIndividualCustomerResp")
    protected SearchNonIndividualCustomerResp searchNonIndividualCustomerResp;

    /**
     * Gets the value of the searchNonIndividualCustomerResp property.
     * 
     * @return
     *     possible object is
     *     {@link SearchNonIndividualCustomerResp }
     *     
     */
    public SearchNonIndividualCustomerResp getSearchNonIndividualCustomerResp() {
        return searchNonIndividualCustomerResp;
    }

    /**
     * Sets the value of the searchNonIndividualCustomerResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchNonIndividualCustomerResp }
     *     
     */
    public void setSearchNonIndividualCustomerResp(SearchNonIndividualCustomerResp value) {
        this.searchNonIndividualCustomerResp = value;
    }

}
