
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchcustresults.SearchCustResults;


/**
 * <p>Java class for SearchCustomerBySCVIDResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustomerBySCVIDResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustResults" type="{http://scv.mce.barclays.com/scv/SearchCustResults}SearchCustResults" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustomerBySCVIDResponse", propOrder = {
    "searchCustResults"
})
public class SearchCustomerBySCVIDResponse {

    @XmlElement(name = "SearchCustResults")
    protected SearchCustResults searchCustResults;

    /**
     * Gets the value of the searchCustResults property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCustResults }
     *     
     */
    public SearchCustResults getSearchCustResults() {
        return searchCustResults;
    }

    /**
     * Sets the value of the searchCustResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCustResults }
     *     
     */
    public void setSearchCustResults(SearchCustResults value) {
        this.searchCustResults = value;
    }

}
