
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.updatemandateduserstatusreq.UpdateMandatedUserStatusReq;


/**
 * <p>Java class for UpdateMandatedUserStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateMandatedUserStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateMandatedUserStatusReq" type="{http://scv.mce.barclays.com/scv/UpdateMandatedUserStatusReq}UpdateMandatedUserStatusReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateMandatedUserStatus", propOrder = {
    "updateMandatedUserStatusReq"
})
public class UpdateMandatedUserStatus {

    @XmlElement(name = "UpdateMandatedUserStatusReq")
    protected UpdateMandatedUserStatusReq updateMandatedUserStatusReq;

    /**
     * Gets the value of the updateMandatedUserStatusReq property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateMandatedUserStatusReq }
     *     
     */
    public UpdateMandatedUserStatusReq getUpdateMandatedUserStatusReq() {
        return updateMandatedUserStatusReq;
    }

    /**
     * Sets the value of the updateMandatedUserStatusReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateMandatedUserStatusReq }
     *     
     */
    public void setUpdateMandatedUserStatusReq(UpdateMandatedUserStatusReq value) {
        this.updateMandatedUserStatusReq = value;
    }

}
