
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.linkcustomerreq.LinkCustomerReq;


/**
 * <p>Java class for LinkCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkCustomerReq" type="{http://scv.mce.barclays.com/match/LinkCustomerReq}LinkCustomerReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkCustomer", propOrder = {
    "linkCustomerReq"
})
public class LinkCustomer {

    @XmlElement(name = "LinkCustomerReq")
    protected LinkCustomerReq linkCustomerReq;

    /**
     * Gets the value of the linkCustomerReq property.
     * 
     * @return
     *     possible object is
     *     {@link LinkCustomerReq }
     *     
     */
    public LinkCustomerReq getLinkCustomerReq() {
        return linkCustomerReq;
    }

    /**
     * Sets the value of the linkCustomerReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkCustomerReq }
     *     
     */
    public void setLinkCustomerReq(LinkCustomerReq value) {
        this.linkCustomerReq = value;
    }

}
