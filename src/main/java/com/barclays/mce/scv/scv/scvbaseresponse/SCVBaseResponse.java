
package com.barclays.mce.scv.scv.scvbaseresponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.retrievelinkedcustomerresp.RetrieveLinkedCustomerResp;
import com.barclays.mce.scv.match.searchmatchresult.SearchMatchResult;
import com.barclays.mce.scv.match.searchpendingmatchresult.SearchPendingMatchResult;
import com.barclays.mce.scv.scv.createcustomerscvidresp.CreateCustomerScvIDResp;
import com.barclays.mce.scv.scv.createmandateduserresp.CreateMandatedUserResp;
import com.barclays.mce.scv.scv.retrievemandateduserlistresp.RetrieveMandatedUserListResp;
import com.barclays.mce.scv.scv.scvserviceheader.SCVRespHeader;
import com.barclays.mce.scv.scv.searchcustresults.SearchCustResults;
import com.barclays.mce.scv.scv.searchmandateduserbyscvidresp.SearchMandatedUserBySCVIDResp;
import com.barclays.mce.scv.scv.searchnonindividualcustomerresp.SearchNonIndividualCustomerResp;
import com.barclays.mce.scv.scv.updatemandateduserresp.UpdateMandatedUserResp;
import com.barclays.mce.scv.scv.updatemandateduserstatusresp.UpdateMandatedUserStatusResp;
import com.barclays.mce.scv.scv.validatecustmobilenumberresp.ValidateCustMobileNumberResp;


/**
 * <p>Java class for SCVBaseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVBaseResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResponseHeader" type="{http://scv.mce.barclays.com/scv/SCVServiceHeader}SCVRespHeader" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVBaseResponse", propOrder = {
    "responseHeader"
})
@XmlSeeAlso({
    ValidateCustMobileNumberResp.class,
    UpdateMandatedUserStatusResp.class,
    UpdateMandatedUserResp.class,
    SearchNonIndividualCustomerResp.class,
    SearchMandatedUserBySCVIDResp.class,
    SearchCustResults.class,
    RetrieveMandatedUserListResp.class,
    CreateMandatedUserResp.class,
    CreateCustomerScvIDResp.class,
    SearchMatchResult.class,
    RetrieveLinkedCustomerResp.class,
    SearchPendingMatchResult.class
})
public class SCVBaseResponse {

    @XmlElement(name = "ResponseHeader", required = true)
    protected SCVRespHeader responseHeader;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link SCVRespHeader }
     *     
     */
    public SCVRespHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVRespHeader }
     *     
     */
    public void setResponseHeader(SCVRespHeader value) {
        this.responseHeader = value;
    }

}
