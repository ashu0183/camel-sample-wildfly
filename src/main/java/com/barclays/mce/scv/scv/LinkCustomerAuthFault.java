
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverror.SCVError;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkCustomerAuth_fault" type="{http://scv.mce.barclays.com/scv/SCVError}SCVError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "linkCustomerAuthFault"
})
@XmlRootElement(name = "LinkCustomerAuth_fault")
public class LinkCustomerAuthFault {

    @XmlElement(name = "LinkCustomerAuth_fault", required = true)
    protected SCVError linkCustomerAuthFault;

    /**
     * Gets the value of the linkCustomerAuthFault property.
     * 
     * @return
     *     possible object is
     *     {@link SCVError }
     *     
     */
    public SCVError getLinkCustomerAuthFault() {
        return linkCustomerAuthFault;
    }

    /**
     * Sets the value of the linkCustomerAuthFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVError }
     *     
     */
    public void setLinkCustomerAuthFault(SCVError value) {
        this.linkCustomerAuthFault = value;
    }

}
