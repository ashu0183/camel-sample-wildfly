
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverror.SCVError;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateCustomerScvID_fault" type="{http://scv.mce.barclays.com/scv/SCVError}SCVError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createCustomerScvIDFault"
})
@XmlRootElement(name = "CreateCustomerScvID_fault")
public class CreateCustomerScvIDFault {

    @XmlElement(name = "CreateCustomerScvID_fault", required = true)
    protected SCVError createCustomerScvIDFault;

    /**
     * Gets the value of the createCustomerScvIDFault property.
     * 
     * @return
     *     possible object is
     *     {@link SCVError }
     *     
     */
    public SCVError getCreateCustomerScvIDFault() {
        return createCustomerScvIDFault;
    }

    /**
     * Sets the value of the createCustomerScvIDFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVError }
     *     
     */
    public void setCreateCustomerScvIDFault(SCVError value) {
        this.createCustomerScvIDFault = value;
    }

}
