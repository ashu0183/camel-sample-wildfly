
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchmandateduserbyscvidresp.SearchMandatedUserBySCVIDResp;


/**
 * <p>Java class for SearchMandatedUserBySCVIDResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMandatedUserBySCVIDResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchMandatedUserBySCVIDResp" type="{http://scv.mce.barclays.com/scv/SearchMandatedUserBySCVIDResp}SearchMandatedUserBySCVIDResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMandatedUserBySCVIDResponse", propOrder = {
    "searchMandatedUserBySCVIDResp"
})
public class SearchMandatedUserBySCVIDResponse {

    @XmlElement(name = "SearchMandatedUserBySCVIDResp")
    protected SearchMandatedUserBySCVIDResp searchMandatedUserBySCVIDResp;

    /**
     * Gets the value of the searchMandatedUserBySCVIDResp property.
     * 
     * @return
     *     possible object is
     *     {@link SearchMandatedUserBySCVIDResp }
     *     
     */
    public SearchMandatedUserBySCVIDResp getSearchMandatedUserBySCVIDResp() {
        return searchMandatedUserBySCVIDResp;
    }

    /**
     * Sets the value of the searchMandatedUserBySCVIDResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMandatedUserBySCVIDResp }
     *     
     */
    public void setSearchMandatedUserBySCVIDResp(SearchMandatedUserBySCVIDResp value) {
        this.searchMandatedUserBySCVIDResp = value;
    }

}
