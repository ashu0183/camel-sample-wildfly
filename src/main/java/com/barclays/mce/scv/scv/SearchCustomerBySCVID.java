
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchcustbyscvidreq.SearchCustBySCVIDReq;


/**
 * <p>Java class for SearchCustomerBySCVID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustomerBySCVID"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustBySCVIDReq" type="{http://scv.mce.barclays.com/scv/SearchCustBySCVIDReq}SearchCustBySCVIDReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustomerBySCVID", propOrder = {
    "searchCustBySCVIDReq"
})
public class SearchCustomerBySCVID {

    @XmlElement(name = "SearchCustBySCVIDReq")
    protected SearchCustBySCVIDReq searchCustBySCVIDReq;

    /**
     * Gets the value of the searchCustBySCVIDReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCustBySCVIDReq }
     *     
     */
    public SearchCustBySCVIDReq getSearchCustBySCVIDReq() {
        return searchCustBySCVIDReq;
    }

    /**
     * Sets the value of the searchCustBySCVIDReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCustBySCVIDReq }
     *     
     */
    public void setSearchCustBySCVIDReq(SearchCustBySCVIDReq value) {
        this.searchCustBySCVIDReq = value;
    }

}
