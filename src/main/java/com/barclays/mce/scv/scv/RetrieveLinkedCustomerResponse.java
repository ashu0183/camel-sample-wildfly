
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.retrievelinkedcustomerresp.RetrieveLinkedCustomerResp;


/**
 * <p>Java class for RetrieveLinkedCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveLinkedCustomerResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetrieveLinkedCustomerResp" type="{http://scv.mce.barclays.com/match/RetrieveLinkedCustomerResp}RetrieveLinkedCustomerResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveLinkedCustomerResponse", propOrder = {
    "retrieveLinkedCustomerResp"
})
public class RetrieveLinkedCustomerResponse {

    @XmlElement(name = "RetrieveLinkedCustomerResp")
    protected RetrieveLinkedCustomerResp retrieveLinkedCustomerResp;

    /**
     * Gets the value of the retrieveLinkedCustomerResp property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveLinkedCustomerResp }
     *     
     */
    public RetrieveLinkedCustomerResp getRetrieveLinkedCustomerResp() {
        return retrieveLinkedCustomerResp;
    }

    /**
     * Sets the value of the retrieveLinkedCustomerResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveLinkedCustomerResp }
     *     
     */
    public void setRetrieveLinkedCustomerResp(RetrieveLinkedCustomerResp value) {
        this.retrieveLinkedCustomerResp = value;
    }

}
