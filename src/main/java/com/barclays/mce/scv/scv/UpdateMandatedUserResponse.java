
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.updatemandateduserresp.UpdateMandatedUserResp;


/**
 * <p>Java class for UpdateMandatedUserResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateMandatedUserResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateMandatedUserResp" type="{http://scv.mce.barclays.com/scv/UpdateMandatedUserResp}UpdateMandatedUserResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateMandatedUserResponse", propOrder = {
    "updateMandatedUserResp"
})
public class UpdateMandatedUserResponse {

    @XmlElement(name = "UpdateMandatedUserResp")
    protected UpdateMandatedUserResp updateMandatedUserResp;

    /**
     * Gets the value of the updateMandatedUserResp property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateMandatedUserResp }
     *     
     */
    public UpdateMandatedUserResp getUpdateMandatedUserResp() {
        return updateMandatedUserResp;
    }

    /**
     * Sets the value of the updateMandatedUserResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateMandatedUserResp }
     *     
     */
    public void setUpdateMandatedUserResp(UpdateMandatedUserResp value) {
        this.updateMandatedUserResp = value;
    }

}
