
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for LinkCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkCustomerResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkCustomerResponse" type="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkCustomerResponse", propOrder = {
    "linkCustomerResponse"
})
public class LinkCustomerResponse {

    @XmlElement(name = "LinkCustomerResponse")
    protected SCVBaseResponse linkCustomerResponse;

    /**
     * Gets the value of the linkCustomerResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SCVBaseResponse }
     *     
     */
    public SCVBaseResponse getLinkCustomerResponse() {
        return linkCustomerResponse;
    }

    /**
     * Sets the value of the linkCustomerResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVBaseResponse }
     *     
     */
    public void setLinkCustomerResponse(SCVBaseResponse value) {
        this.linkCustomerResponse = value;
    }

}
