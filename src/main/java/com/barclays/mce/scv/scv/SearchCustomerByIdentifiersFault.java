
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverror.SCVError;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustomerByIdentifiers_fault" type="{http://scv.mce.barclays.com/scv/SCVError}SCVError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchCustomerByIdentifiersFault"
})
@XmlRootElement(name = "SearchCustomerByIdentifiers_fault")
public class SearchCustomerByIdentifiersFault {

    @XmlElement(name = "SearchCustomerByIdentifiers_fault", required = true)
    protected SCVError searchCustomerByIdentifiersFault;

    /**
     * Gets the value of the searchCustomerByIdentifiersFault property.
     * 
     * @return
     *     possible object is
     *     {@link SCVError }
     *     
     */
    public SCVError getSearchCustomerByIdentifiersFault() {
        return searchCustomerByIdentifiersFault;
    }

    /**
     * Sets the value of the searchCustomerByIdentifiersFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVError }
     *     
     */
    public void setSearchCustomerByIdentifiersFault(SCVError value) {
        this.searchCustomerByIdentifiersFault = value;
    }

}
