
package com.barclays.mce.scv.scv.createcustomerscvidreq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaserequest.SCVBaseRequest;
import com.barclays.mce.scv.scv.scvdetails.SCVDetails;


/**
 * <p>Java class for CreateCustomerScvIDReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCustomerScvIDReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseRequest}SCVBaseRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SCVDetails" type="{http://scv.mce.barclays.com/scv/SCVDetails}SCVDetails" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCustomerScvIDReq", propOrder = {
    "scvDetails"
})
public class CreateCustomerScvIDReq
    extends SCVBaseRequest
{

    @XmlElement(name = "SCVDetails")
    protected SCVDetails scvDetails;

    /**
     * Gets the value of the scvDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SCVDetails }
     *     
     */
    public SCVDetails getSCVDetails() {
        return scvDetails;
    }

    /**
     * Sets the value of the scvDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVDetails }
     *     
     */
    public void setSCVDetails(SCVDetails value) {
        this.scvDetails = value;
    }

}
