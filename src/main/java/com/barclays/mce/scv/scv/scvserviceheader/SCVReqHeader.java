
package com.barclays.mce.scv.scv.scvserviceheader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SCVReqHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVReqHeader"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BusinessID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OriginatingChannel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ServiceDateTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TransactionReferenceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVReqHeader", propOrder = {
    "businessID",
    "originatingChannel",
    "userID",
    "serviceDateTime",
    "transactionReferenceNo"
})
public class SCVReqHeader {

    @XmlElement(name = "BusinessID", required = true)
    protected String businessID;
    @XmlElement(name = "OriginatingChannel", required = true)
    protected String originatingChannel;
    @XmlElement(name = "UserID", required = true)
    protected String userID;
    @XmlElement(name = "ServiceDateTime", required = true)
    protected String serviceDateTime;
    @XmlElement(name = "TransactionReferenceNo")
    protected String transactionReferenceNo;

    /**
     * Gets the value of the businessID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessID() {
        return businessID;
    }

    /**
     * Sets the value of the businessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessID(String value) {
        this.businessID = value;
    }

    /**
     * Gets the value of the originatingChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingChannel() {
        return originatingChannel;
    }

    /**
     * Sets the value of the originatingChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingChannel(String value) {
        this.originatingChannel = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the serviceDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDateTime() {
        return serviceDateTime;
    }

    /**
     * Sets the value of the serviceDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDateTime(String value) {
        this.serviceDateTime = value;
    }

    /**
     * Gets the value of the transactionReferenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionReferenceNo() {
        return transactionReferenceNo;
    }

    /**
     * Sets the value of the transactionReferenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionReferenceNo(String value) {
        this.transactionReferenceNo = value;
    }

}
