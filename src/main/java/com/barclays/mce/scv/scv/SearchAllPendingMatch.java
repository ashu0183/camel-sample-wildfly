
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.searchallpendingmatchreq.SearchAllPendingMatchReq;


/**
 * <p>Java class for SearchAllPendingMatch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchAllPendingMatch"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchAllPendingMatchReq" type="{http://scv.mce.barclays.com/match/SearchAllPendingMatchReq}SearchAllPendingMatchReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchAllPendingMatch", propOrder = {
    "searchAllPendingMatchReq"
})
public class SearchAllPendingMatch {

    @XmlElement(name = "SearchAllPendingMatchReq")
    protected SearchAllPendingMatchReq searchAllPendingMatchReq;

    /**
     * Gets the value of the searchAllPendingMatchReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchAllPendingMatchReq }
     *     
     */
    public SearchAllPendingMatchReq getSearchAllPendingMatchReq() {
        return searchAllPendingMatchReq;
    }

    /**
     * Sets the value of the searchAllPendingMatchReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchAllPendingMatchReq }
     *     
     */
    public void setSearchAllPendingMatchReq(SearchAllPendingMatchReq value) {
        this.searchAllPendingMatchReq = value;
    }

}
