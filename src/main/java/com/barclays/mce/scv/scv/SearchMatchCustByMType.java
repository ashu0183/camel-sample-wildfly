
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.searchmatchcustbymtypereq.SearchMatchCustByMTypeReq;


/**
 * <p>Java class for SearchMatchCustByMType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMatchCustByMType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchMatchCustByMTypeReq" type="{http://scv.mce.barclays.com/match/SearchMatchCustByMTypeReq}SearchMatchCustByMTypeReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMatchCustByMType", propOrder = {
    "searchMatchCustByMTypeReq"
})
public class SearchMatchCustByMType {

    @XmlElement(name = "SearchMatchCustByMTypeReq")
    protected SearchMatchCustByMTypeReq searchMatchCustByMTypeReq;

    /**
     * Gets the value of the searchMatchCustByMTypeReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchMatchCustByMTypeReq }
     *     
     */
    public SearchMatchCustByMTypeReq getSearchMatchCustByMTypeReq() {
        return searchMatchCustByMTypeReq;
    }

    /**
     * Sets the value of the searchMatchCustByMTypeReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMatchCustByMTypeReq }
     *     
     */
    public void setSearchMatchCustByMTypeReq(SearchMatchCustByMTypeReq value) {
        this.searchMatchCustByMTypeReq = value;
    }

}
