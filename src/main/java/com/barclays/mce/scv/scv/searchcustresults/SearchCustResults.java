
package com.barclays.mce.scv.scv.searchcustresults;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;
import com.barclays.mce.scv.scv.scvdetails.SCVDetails;


/**
 * <p>Java class for SearchCustResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustResults"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TotalRecordCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="SCVDetails" type="{http://scv.mce.barclays.com/scv/SCVDetails}SCVDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustResults", propOrder = {
    "totalRecordCount",
    "scvDetails"
})
public class SearchCustResults
    extends SCVBaseResponse
{

    @XmlElement(name = "TotalRecordCount")
    protected Integer totalRecordCount;
    @XmlElement(name = "SCVDetails")
    protected List<SCVDetails> scvDetails;

    /**
     * Gets the value of the totalRecordCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * Sets the value of the totalRecordCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalRecordCount(Integer value) {
        this.totalRecordCount = value;
    }

    /**
     * Gets the value of the scvDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scvDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCVDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCVDetails }
     * 
     * 
     */
    public List<SCVDetails> getSCVDetails() {
        if (scvDetails == null) {
            scvDetails = new ArrayList<SCVDetails>();
        }
        return this.scvDetails;
    }

}
