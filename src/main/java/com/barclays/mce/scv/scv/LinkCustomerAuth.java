
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.linkcustomerauthreq.LinkCustomerAuthReq;


/**
 * <p>Java class for LinkCustomerAuth complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkCustomerAuth"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkCustomerAuthReq" type="{http://scv.mce.barclays.com/match/LinkCustomerAuthReq}LinkCustomerAuthReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkCustomerAuth", propOrder = {
    "linkCustomerAuthReq"
})
public class LinkCustomerAuth {

    @XmlElement(name = "LinkCustomerAuthReq")
    protected LinkCustomerAuthReq linkCustomerAuthReq;

    /**
     * Gets the value of the linkCustomerAuthReq property.
     * 
     * @return
     *     possible object is
     *     {@link LinkCustomerAuthReq }
     *     
     */
    public LinkCustomerAuthReq getLinkCustomerAuthReq() {
        return linkCustomerAuthReq;
    }

    /**
     * Sets the value of the linkCustomerAuthReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkCustomerAuthReq }
     *     
     */
    public void setLinkCustomerAuthReq(LinkCustomerAuthReq value) {
        this.linkCustomerAuthReq = value;
    }

}
