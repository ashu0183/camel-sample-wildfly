
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.validatecustmobilenumberresp.ValidateCustMobileNumberResp;


/**
 * <p>Java class for ValidateCustMobileNumberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateCustMobileNumberResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ValidateCustMobileNumberResp" type="{http://scv.mce.barclays.com/scv/ValidateCustMobileNumberResp}ValidateCustMobileNumberResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateCustMobileNumberResponse", propOrder = {
    "validateCustMobileNumberResp"
})
public class ValidateCustMobileNumberResponse {

    @XmlElement(name = "ValidateCustMobileNumberResp")
    protected ValidateCustMobileNumberResp validateCustMobileNumberResp;

    /**
     * Gets the value of the validateCustMobileNumberResp property.
     * 
     * @return
     *     possible object is
     *     {@link ValidateCustMobileNumberResp }
     *     
     */
    public ValidateCustMobileNumberResp getValidateCustMobileNumberResp() {
        return validateCustMobileNumberResp;
    }

    /**
     * Sets the value of the validateCustMobileNumberResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateCustMobileNumberResp }
     *     
     */
    public void setValidateCustMobileNumberResp(ValidateCustMobileNumberResp value) {
        this.validateCustMobileNumberResp = value;
    }

}
