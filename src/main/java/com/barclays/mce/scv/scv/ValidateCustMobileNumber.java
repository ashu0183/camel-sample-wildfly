
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.validatecustmobilenumberreq.ValidateCustMobileNumberReq;


/**
 * <p>Java class for ValidateCustMobileNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateCustMobileNumber"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ValidateCustMobileNumberReq" type="{http://scv.mce.barclays.com/scv/ValidateCustMobileNumberReq}ValidateCustMobileNumberReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateCustMobileNumber", propOrder = {
    "validateCustMobileNumberReq"
})
public class ValidateCustMobileNumber {

    @XmlElement(name = "ValidateCustMobileNumberReq")
    protected ValidateCustMobileNumberReq validateCustMobileNumberReq;

    /**
     * Gets the value of the validateCustMobileNumberReq property.
     * 
     * @return
     *     possible object is
     *     {@link ValidateCustMobileNumberReq }
     *     
     */
    public ValidateCustMobileNumberReq getValidateCustMobileNumberReq() {
        return validateCustMobileNumberReq;
    }

    /**
     * Sets the value of the validateCustMobileNumberReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateCustMobileNumberReq }
     *     
     */
    public void setValidateCustMobileNumberReq(ValidateCustMobileNumberReq value) {
        this.validateCustMobileNumberReq = value;
    }

}
