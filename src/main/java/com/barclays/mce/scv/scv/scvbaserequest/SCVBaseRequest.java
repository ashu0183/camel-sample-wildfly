
package com.barclays.mce.scv.scv.scvbaserequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.linkcustomerauthreq.LinkCustomerAuthReq;
import com.barclays.mce.scv.match.linkcustomerreq.LinkCustomerReq;
import com.barclays.mce.scv.match.retrievelinkedcustomerreq.RetrieveLinkedCustomerReq;
import com.barclays.mce.scv.match.searchallpendingmatchreq.SearchAllPendingMatchReq;
import com.barclays.mce.scv.match.searchmatchcustbycifreq.SearchMatchCustByCIFReq;
import com.barclays.mce.scv.match.searchmatchcustbymtypereq.SearchMatchCustByMTypeReq;
import com.barclays.mce.scv.match.unlinkcustomerreq.UnlinkCustomerReq;
import com.barclays.mce.scv.scv.createcustomerscvidreq.CreateCustomerScvIDReq;
import com.barclays.mce.scv.scv.createmandateduserreq.CreateMandatedUserReq;
import com.barclays.mce.scv.scv.retrievemandateduserlistreq.RetrieveMandatedUserListReq;
import com.barclays.mce.scv.scv.scvserviceheader.SCVReqHeader;
import com.barclays.mce.scv.scv.searchcustbyidentifiersreq.SearchCustByIdentifiersReq;
import com.barclays.mce.scv.scv.searchcustbynamereq.SearchCustByNameReq;
import com.barclays.mce.scv.scv.searchcustbyppidreq.SearchCustByPPIdReq;
import com.barclays.mce.scv.scv.searchcustbyscvidreq.SearchCustBySCVIDReq;
import com.barclays.mce.scv.scv.searchcustomerreq.SearchCustomerReq;
import com.barclays.mce.scv.scv.searchmandateduserbyscvidreq.SearchMandatedUserBySCVIDReq;
import com.barclays.mce.scv.scv.searchnonindividualcustomerreq.SearchNonIndividualCustomerReq;
import com.barclays.mce.scv.scv.updatemandateduserreq.UpdateMandatedUserReq;
import com.barclays.mce.scv.scv.updatemandateduserstatusreq.UpdateMandatedUserStatusReq;
import com.barclays.mce.scv.scv.validatecustmobilenumberreq.ValidateCustMobileNumberReq;


/**
 * <p>Java class for SCVBaseRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVBaseRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequestHeader" type="{http://scv.mce.barclays.com/scv/SCVServiceHeader}SCVReqHeader" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVBaseRequest", propOrder = {
    "requestHeader"
})
@XmlSeeAlso({
    ValidateCustMobileNumberReq.class,
    UpdateMandatedUserStatusReq.class,
    UpdateMandatedUserReq.class,
    SearchNonIndividualCustomerReq.class,
    SearchMandatedUserBySCVIDReq.class,
    SearchCustomerReq.class,
    SearchCustBySCVIDReq.class,
    SearchCustByPPIdReq.class,
    SearchCustByNameReq.class,
    SearchCustByIdentifiersReq.class,
    RetrieveMandatedUserListReq.class,
    CreateMandatedUserReq.class,
    CreateCustomerScvIDReq.class,
    UnlinkCustomerReq.class,
    RetrieveLinkedCustomerReq.class,
    SearchMatchCustByCIFReq.class,
    SearchAllPendingMatchReq.class,
    SearchMatchCustByMTypeReq.class,
    LinkCustomerReq.class,
    LinkCustomerAuthReq.class
})
public class SCVBaseRequest {

    @XmlElement(name = "RequestHeader", required = true)
    protected SCVReqHeader requestHeader;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link SCVReqHeader }
     *     
     */
    public SCVReqHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVReqHeader }
     *     
     */
    public void setRequestHeader(SCVReqHeader value) {
        this.requestHeader = value;
    }

}
