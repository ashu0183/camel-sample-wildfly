
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.createcustomerscvidreq.CreateCustomerScvIDReq;


/**
 * <p>Java class for CreateCustomerScvID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCustomerScvID"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateCustomerScvIDReq" type="{http://scv.mce.barclays.com/scv/CreateCustomerScvIDReq}CreateCustomerScvIDReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCustomerScvID", propOrder = {
    "createCustomerScvIDReq"
})
public class CreateCustomerScvID {

    @XmlElement(name = "CreateCustomerScvIDReq")
    protected CreateCustomerScvIDReq createCustomerScvIDReq;

    /**
     * Gets the value of the createCustomerScvIDReq property.
     * 
     * @return
     *     possible object is
     *     {@link CreateCustomerScvIDReq }
     *     
     */
    public CreateCustomerScvIDReq getCreateCustomerScvIDReq() {
        return createCustomerScvIDReq;
    }

    /**
     * Sets the value of the createCustomerScvIDReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateCustomerScvIDReq }
     *     
     */
    public void setCreateCustomerScvIDReq(CreateCustomerScvIDReq value) {
        this.createCustomerScvIDReq = value;
    }

}
