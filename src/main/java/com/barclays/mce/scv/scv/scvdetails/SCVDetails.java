
package com.barclays.mce.scv.scv.scvdetails;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.ppdetails.PPDetails;
import com.barclays.mce.scv.scv.scvbasedatatypes.CustomerIdentifyDetails;


/**
 * <p>Java class for SCVDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SCVID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="OldSCVID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="SCVIDChangedFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="MobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CustomerIdentifierDetails" type="{http://scv.mce.barclays.com/scv/SCVBaseDataTypes}CustomerIdentifyDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="Salutation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CustomerFullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CustomerSegment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="PPDetails" type="{http://scv.mce.barclays.com/scv/PPDetails}PPDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="DelqAccountsFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="PendingAppsFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="DialingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="EntitySCVID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVDetails", propOrder = {
    "scvid",
    "oldSCVID",
    "scvidChangedFlag",
    "firstName",
    "lastName",
    "mobileNumber",
    "dateOfBirth",
    "customerIdentifierDetails",
    "status",
    "salutation",
    "customerFullName",
    "customerSegment",
    "ppDetails",
    "delqAccountsFlag",
    "pendingAppsFlag",
    "dialingCode",
    "emailAddress",
    "entitySCVID"
})
public class SCVDetails {

    @XmlElement(name = "SCVID")
    protected String scvid;
    @XmlElement(name = "OldSCVID")
    protected String oldSCVID;
    @XmlElement(name = "SCVIDChangedFlag")
    protected Boolean scvidChangedFlag;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "MobileNumber")
    protected String mobileNumber;
    @XmlElement(name = "DateOfBirth")
    protected String dateOfBirth;
    @XmlElement(name = "CustomerIdentifierDetails")
    protected List<CustomerIdentifyDetails> customerIdentifierDetails;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "Salutation")
    protected String salutation;
    @XmlElement(name = "CustomerFullName")
    protected String customerFullName;
    @XmlElement(name = "CustomerSegment")
    protected String customerSegment;
    @XmlElement(name = "PPDetails")
    protected List<PPDetails> ppDetails;
    @XmlElement(name = "DelqAccountsFlag")
    protected Boolean delqAccountsFlag;
    @XmlElement(name = "PendingAppsFlag")
    protected Boolean pendingAppsFlag;
    @XmlElement(name = "DialingCode")
    protected String dialingCode;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "EntitySCVID")
    protected String entitySCVID;

    /**
     * Gets the value of the scvid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCVID() {
        return scvid;
    }

    /**
     * Sets the value of the scvid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCVID(String value) {
        this.scvid = value;
    }

    /**
     * Gets the value of the oldSCVID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldSCVID() {
        return oldSCVID;
    }

    /**
     * Sets the value of the oldSCVID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldSCVID(String value) {
        this.oldSCVID = value;
    }

    /**
     * Gets the value of the scvidChangedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSCVIDChangedFlag() {
        return scvidChangedFlag;
    }

    /**
     * Sets the value of the scvidChangedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSCVIDChangedFlag(Boolean value) {
        this.scvidChangedFlag = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfBirth(String value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the customerIdentifierDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerIdentifierDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerIdentifierDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerIdentifyDetails }
     * 
     * 
     */
    public List<CustomerIdentifyDetails> getCustomerIdentifierDetails() {
        if (customerIdentifierDetails == null) {
            customerIdentifierDetails = new ArrayList<CustomerIdentifyDetails>();
        }
        return this.customerIdentifierDetails;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the salutation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Sets the value of the salutation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalutation(String value) {
        this.salutation = value;
    }

    /**
     * Gets the value of the customerFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerFullName() {
        return customerFullName;
    }

    /**
     * Sets the value of the customerFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerFullName(String value) {
        this.customerFullName = value;
    }

    /**
     * Gets the value of the customerSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSegment() {
        return customerSegment;
    }

    /**
     * Sets the value of the customerSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSegment(String value) {
        this.customerSegment = value;
    }

    /**
     * Gets the value of the ppDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ppDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPPDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PPDetails }
     * 
     * 
     */
    public List<PPDetails> getPPDetails() {
        if (ppDetails == null) {
            ppDetails = new ArrayList<PPDetails>();
        }
        return this.ppDetails;
    }

    /**
     * Gets the value of the delqAccountsFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDelqAccountsFlag() {
        return delqAccountsFlag;
    }

    /**
     * Sets the value of the delqAccountsFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDelqAccountsFlag(Boolean value) {
        this.delqAccountsFlag = value;
    }

    /**
     * Gets the value of the pendingAppsFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPendingAppsFlag() {
        return pendingAppsFlag;
    }

    /**
     * Sets the value of the pendingAppsFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPendingAppsFlag(Boolean value) {
        this.pendingAppsFlag = value;
    }

    /**
     * Gets the value of the dialingCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDialingCode() {
        return dialingCode;
    }

    /**
     * Sets the value of the dialingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDialingCode(String value) {
        this.dialingCode = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the entitySCVID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitySCVID() {
        return entitySCVID;
    }

    /**
     * Sets the value of the entitySCVID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitySCVID(String value) {
        this.entitySCVID = value;
    }

}
