
package com.barclays.mce.scv.scv.scvserviceheader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverrorlist.SCVErrorList;


/**
 * <p>Java class for SCVRespHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVRespHeader"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SCVErrorList" type="{http://scv.mce.barclays.com/scv/SCVErrorList}SCVErrorList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVRespHeader", propOrder = {
    "serviceResponseCode",
    "scvErrorList"
})
public class SCVRespHeader {

    @XmlElement(name = "ServiceResponseCode", required = true)
    protected String serviceResponseCode;
    @XmlElement(name = "SCVErrorList")
    protected SCVErrorList scvErrorList;

    /**
     * Gets the value of the serviceResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceResponseCode() {
        return serviceResponseCode;
    }

    /**
     * Sets the value of the serviceResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceResponseCode(String value) {
        this.serviceResponseCode = value;
    }

    /**
     * Gets the value of the scvErrorList property.
     * 
     * @return
     *     possible object is
     *     {@link SCVErrorList }
     *     
     */
    public SCVErrorList getSCVErrorList() {
        return scvErrorList;
    }

    /**
     * Sets the value of the scvErrorList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVErrorList }
     *     
     */
    public void setSCVErrorList(SCVErrorList value) {
        this.scvErrorList = value;
    }

}
