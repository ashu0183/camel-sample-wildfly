
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchmandateduserbyscvidreq.SearchMandatedUserBySCVIDReq;


/**
 * <p>Java class for SearchMandatedUserBySCVID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMandatedUserBySCVID"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchMandatedUserBySCVIDReq" type="{http://scv.mce.barclays.com/scv/SearchMandatedUserBySCVIDReq}SearchMandatedUserBySCVIDReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMandatedUserBySCVID", propOrder = {
    "searchMandatedUserBySCVIDReq"
})
public class SearchMandatedUserBySCVID {

    @XmlElement(name = "SearchMandatedUserBySCVIDReq")
    protected SearchMandatedUserBySCVIDReq searchMandatedUserBySCVIDReq;

    /**
     * Gets the value of the searchMandatedUserBySCVIDReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchMandatedUserBySCVIDReq }
     *     
     */
    public SearchMandatedUserBySCVIDReq getSearchMandatedUserBySCVIDReq() {
        return searchMandatedUserBySCVIDReq;
    }

    /**
     * Sets the value of the searchMandatedUserBySCVIDReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMandatedUserBySCVIDReq }
     *     
     */
    public void setSearchMandatedUserBySCVIDReq(SearchMandatedUserBySCVIDReq value) {
        this.searchMandatedUserBySCVIDReq = value;
    }

}
