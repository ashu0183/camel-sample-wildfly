
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.updatemandateduserstatusresp.UpdateMandatedUserStatusResp;


/**
 * <p>Java class for UpdateMandatedUserStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateMandatedUserStatusResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateMandatedUserStatusResp" type="{http://scv.mce.barclays.com/scv/UpdateMandatedUserStatusResp}UpdateMandatedUserStatusResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateMandatedUserStatusResponse", propOrder = {
    "updateMandatedUserStatusResp"
})
public class UpdateMandatedUserStatusResponse {

    @XmlElement(name = "UpdateMandatedUserStatusResp")
    protected UpdateMandatedUserStatusResp updateMandatedUserStatusResp;

    /**
     * Gets the value of the updateMandatedUserStatusResp property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateMandatedUserStatusResp }
     *     
     */
    public UpdateMandatedUserStatusResp getUpdateMandatedUserStatusResp() {
        return updateMandatedUserStatusResp;
    }

    /**
     * Sets the value of the updateMandatedUserStatusResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateMandatedUserStatusResp }
     *     
     */
    public void setUpdateMandatedUserStatusResp(UpdateMandatedUserStatusResp value) {
        this.updateMandatedUserStatusResp = value;
    }

}
