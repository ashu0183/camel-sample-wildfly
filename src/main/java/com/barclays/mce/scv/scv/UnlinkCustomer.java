
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.unlinkcustomerreq.UnlinkCustomerReq;


/**
 * <p>Java class for UnlinkCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnlinkCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnlinkCustomerReq" type="{http://scv.mce.barclays.com/match/UnlinkCustomerReq}UnlinkCustomerReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnlinkCustomer", propOrder = {
    "unlinkCustomerReq"
})
public class UnlinkCustomer {

    @XmlElement(name = "UnlinkCustomerReq")
    protected UnlinkCustomerReq unlinkCustomerReq;

    /**
     * Gets the value of the unlinkCustomerReq property.
     * 
     * @return
     *     possible object is
     *     {@link UnlinkCustomerReq }
     *     
     */
    public UnlinkCustomerReq getUnlinkCustomerReq() {
        return unlinkCustomerReq;
    }

    /**
     * Sets the value of the unlinkCustomerReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnlinkCustomerReq }
     *     
     */
    public void setUnlinkCustomerReq(UnlinkCustomerReq value) {
        this.unlinkCustomerReq = value;
    }

}
