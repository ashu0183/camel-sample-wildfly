
package com.barclays.mce.scv.scv;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.barclays.mce.scv.scv package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateCustomerScvID_QNAME = new QName("http://scv.mce.barclays.com/scv/", "CreateCustomerScvID");
    private final static QName _CreateCustomerScvIDResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "CreateCustomerScvIDResponse");
    private final static QName _CreateMandatedUser_QNAME = new QName("http://scv.mce.barclays.com/scv/", "CreateMandatedUser");
    private final static QName _CreateMandatedUserResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "CreateMandatedUserResponse");
    private final static QName _LinkCustomer_QNAME = new QName("http://scv.mce.barclays.com/scv/", "LinkCustomer");
    private final static QName _LinkCustomerAuth_QNAME = new QName("http://scv.mce.barclays.com/scv/", "LinkCustomerAuth");
    private final static QName _LinkCustomerAuthResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "LinkCustomerAuthResponse");
    private final static QName _LinkCustomerResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "LinkCustomerResponse");
    private final static QName _RetrieveLinkedCustomer_QNAME = new QName("http://scv.mce.barclays.com/scv/", "RetrieveLinkedCustomer");
    private final static QName _RetrieveLinkedCustomerResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "RetrieveLinkedCustomerResponse");
    private final static QName _RetrieveMandatedUserList_QNAME = new QName("http://scv.mce.barclays.com/scv/", "RetrieveMandatedUserList");
    private final static QName _RetrieveMandatedUserListResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "RetrieveMandatedUserListResponse");
    private final static QName _SearchAllPendingMatch_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchAllPendingMatch");
    private final static QName _SearchAllPendingMatchResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchAllPendingMatchResponse");
    private final static QName _SearchCustomer_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomer");
    private final static QName _SearchCustomerByIdentifiers_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerByIdentifiers");
    private final static QName _SearchCustomerByIdentifiersResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerByIdentifiersResponse");
    private final static QName _SearchCustomerByName_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerByName");
    private final static QName _SearchCustomerByNameResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerByNameResponse");
    private final static QName _SearchCustomerByPPId_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerByPPId");
    private final static QName _SearchCustomerByPPIdResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerByPPIdResponse");
    private final static QName _SearchCustomerBySCVID_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerBySCVID");
    private final static QName _SearchCustomerBySCVIDResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerBySCVIDResponse");
    private final static QName _SearchCustomerResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchCustomerResponse");
    private final static QName _SearchMandatedUserBySCVID_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchMandatedUserBySCVID");
    private final static QName _SearchMandatedUserBySCVIDResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchMandatedUserBySCVIDResponse");
    private final static QName _SearchMatchCustByCIF_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchMatchCustByCIF");
    private final static QName _SearchMatchCustByCIFResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchMatchCustByCIFResponse");
    private final static QName _SearchMatchCustByMType_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchMatchCustByMType");
    private final static QName _SearchMatchCustByMTypeResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchMatchCustByMTypeResponse");
    private final static QName _SearchNonIndividualCustomer_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchNonIndividualCustomer");
    private final static QName _SearchNonIndividualCustomerResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "SearchNonIndividualCustomerResponse");
    private final static QName _UnlinkCustomer_QNAME = new QName("http://scv.mce.barclays.com/scv/", "UnlinkCustomer");
    private final static QName _UnlinkCustomerResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "UnlinkCustomerResponse");
    private final static QName _UpdateMandatedUser_QNAME = new QName("http://scv.mce.barclays.com/scv/", "UpdateMandatedUser");
    private final static QName _UpdateMandatedUserResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "UpdateMandatedUserResponse");
    private final static QName _UpdateMandatedUserStatus_QNAME = new QName("http://scv.mce.barclays.com/scv/", "UpdateMandatedUserStatus");
    private final static QName _UpdateMandatedUserStatusResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "UpdateMandatedUserStatusResponse");
    private final static QName _ValidateCustMobileNumber_QNAME = new QName("http://scv.mce.barclays.com/scv/", "ValidateCustMobileNumber");
    private final static QName _ValidateCustMobileNumberResponse_QNAME = new QName("http://scv.mce.barclays.com/scv/", "ValidateCustMobileNumberResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.barclays.mce.scv.scv
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateCustomerScvID }
     * 
     */
    public CreateCustomerScvID createCreateCustomerScvID() {
        return new CreateCustomerScvID();
    }

    /**
     * Create an instance of {@link CreateCustomerScvIDResponse }
     * 
     */
    public CreateCustomerScvIDResponse createCreateCustomerScvIDResponse() {
        return new CreateCustomerScvIDResponse();
    }

    /**
     * Create an instance of {@link CreateCustomerScvIDFault }
     * 
     */
    public CreateCustomerScvIDFault createCreateCustomerScvIDFault() {
        return new CreateCustomerScvIDFault();
    }

    /**
     * Create an instance of {@link CreateMandatedUser }
     * 
     */
    public CreateMandatedUser createCreateMandatedUser() {
        return new CreateMandatedUser();
    }

    /**
     * Create an instance of {@link CreateMandatedUserResponse }
     * 
     */
    public CreateMandatedUserResponse createCreateMandatedUserResponse() {
        return new CreateMandatedUserResponse();
    }

    /**
     * Create an instance of {@link CreateMandatedUserFault }
     * 
     */
    public CreateMandatedUserFault createCreateMandatedUserFault() {
        return new CreateMandatedUserFault();
    }

    /**
     * Create an instance of {@link LinkCustomer }
     * 
     */
    public LinkCustomer createLinkCustomer() {
        return new LinkCustomer();
    }

    /**
     * Create an instance of {@link LinkCustomerAuth }
     * 
     */
    public LinkCustomerAuth createLinkCustomerAuth() {
        return new LinkCustomerAuth();
    }

    /**
     * Create an instance of {@link LinkCustomerAuthResponse }
     * 
     */
    public LinkCustomerAuthResponse createLinkCustomerAuthResponse() {
        return new LinkCustomerAuthResponse();
    }

    /**
     * Create an instance of {@link LinkCustomerAuthFault }
     * 
     */
    public LinkCustomerAuthFault createLinkCustomerAuthFault() {
        return new LinkCustomerAuthFault();
    }

    /**
     * Create an instance of {@link LinkCustomerResponse }
     * 
     */
    public LinkCustomerResponse createLinkCustomerResponse() {
        return new LinkCustomerResponse();
    }

    /**
     * Create an instance of {@link LinkCustomerFault }
     * 
     */
    public LinkCustomerFault createLinkCustomerFault() {
        return new LinkCustomerFault();
    }

    /**
     * Create an instance of {@link RetrieveLinkedCustFault }
     * 
     */
    public RetrieveLinkedCustFault createRetrieveLinkedCustFault() {
        return new RetrieveLinkedCustFault();
    }

    /**
     * Create an instance of {@link RetrieveLinkedCustomer }
     * 
     */
    public RetrieveLinkedCustomer createRetrieveLinkedCustomer() {
        return new RetrieveLinkedCustomer();
    }

    /**
     * Create an instance of {@link RetrieveLinkedCustomerResponse }
     * 
     */
    public RetrieveLinkedCustomerResponse createRetrieveLinkedCustomerResponse() {
        return new RetrieveLinkedCustomerResponse();
    }

    /**
     * Create an instance of {@link RetrieveMandatedUserList }
     * 
     */
    public RetrieveMandatedUserList createRetrieveMandatedUserList() {
        return new RetrieveMandatedUserList();
    }

    /**
     * Create an instance of {@link RetrieveMandatedUserListResponse }
     * 
     */
    public RetrieveMandatedUserListResponse createRetrieveMandatedUserListResponse() {
        return new RetrieveMandatedUserListResponse();
    }

    /**
     * Create an instance of {@link RetrieveMandatedUserListFault }
     * 
     */
    public RetrieveMandatedUserListFault createRetrieveMandatedUserListFault() {
        return new RetrieveMandatedUserListFault();
    }

    /**
     * Create an instance of {@link SearchAllPendingMatch }
     * 
     */
    public SearchAllPendingMatch createSearchAllPendingMatch() {
        return new SearchAllPendingMatch();
    }

    /**
     * Create an instance of {@link SearchAllPendingMatchResponse }
     * 
     */
    public SearchAllPendingMatchResponse createSearchAllPendingMatchResponse() {
        return new SearchAllPendingMatchResponse();
    }

    /**
     * Create an instance of {@link SearchAllPendingMatchFault }
     * 
     */
    public SearchAllPendingMatchFault createSearchAllPendingMatchFault() {
        return new SearchAllPendingMatchFault();
    }

    /**
     * Create an instance of {@link SearchCustomer }
     * 
     */
    public SearchCustomer createSearchCustomer() {
        return new SearchCustomer();
    }

    /**
     * Create an instance of {@link SearchCustomerByIdentifiers }
     * 
     */
    public SearchCustomerByIdentifiers createSearchCustomerByIdentifiers() {
        return new SearchCustomerByIdentifiers();
    }

    /**
     * Create an instance of {@link SearchCustomerByIdentifiersResponse }
     * 
     */
    public SearchCustomerByIdentifiersResponse createSearchCustomerByIdentifiersResponse() {
        return new SearchCustomerByIdentifiersResponse();
    }

    /**
     * Create an instance of {@link SearchCustomerByIdentifiersFault }
     * 
     */
    public SearchCustomerByIdentifiersFault createSearchCustomerByIdentifiersFault() {
        return new SearchCustomerByIdentifiersFault();
    }

    /**
     * Create an instance of {@link SearchCustomerByName }
     * 
     */
    public SearchCustomerByName createSearchCustomerByName() {
        return new SearchCustomerByName();
    }

    /**
     * Create an instance of {@link SearchCustomerByNameResponse }
     * 
     */
    public SearchCustomerByNameResponse createSearchCustomerByNameResponse() {
        return new SearchCustomerByNameResponse();
    }

    /**
     * Create an instance of {@link SearchCustomerByNameFault }
     * 
     */
    public SearchCustomerByNameFault createSearchCustomerByNameFault() {
        return new SearchCustomerByNameFault();
    }

    /**
     * Create an instance of {@link SearchCustomerByPPId }
     * 
     */
    public SearchCustomerByPPId createSearchCustomerByPPId() {
        return new SearchCustomerByPPId();
    }

    /**
     * Create an instance of {@link SearchCustomerByPPIdResponse }
     * 
     */
    public SearchCustomerByPPIdResponse createSearchCustomerByPPIdResponse() {
        return new SearchCustomerByPPIdResponse();
    }

    /**
     * Create an instance of {@link SearchCustomerByPPIdFault }
     * 
     */
    public SearchCustomerByPPIdFault createSearchCustomerByPPIdFault() {
        return new SearchCustomerByPPIdFault();
    }

    /**
     * Create an instance of {@link SearchCustomerBySCVID }
     * 
     */
    public SearchCustomerBySCVID createSearchCustomerBySCVID() {
        return new SearchCustomerBySCVID();
    }

    /**
     * Create an instance of {@link SearchCustomerBySCVIDResponse }
     * 
     */
    public SearchCustomerBySCVIDResponse createSearchCustomerBySCVIDResponse() {
        return new SearchCustomerBySCVIDResponse();
    }

    /**
     * Create an instance of {@link SearchCustomerBySCVIDFault }
     * 
     */
    public SearchCustomerBySCVIDFault createSearchCustomerBySCVIDFault() {
        return new SearchCustomerBySCVIDFault();
    }

    /**
     * Create an instance of {@link SearchCustomerResponse }
     * 
     */
    public SearchCustomerResponse createSearchCustomerResponse() {
        return new SearchCustomerResponse();
    }

    /**
     * Create an instance of {@link SearchCustomerFault }
     * 
     */
    public SearchCustomerFault createSearchCustomerFault() {
        return new SearchCustomerFault();
    }

    /**
     * Create an instance of {@link SearchMandatedUserBySCVID }
     * 
     */
    public SearchMandatedUserBySCVID createSearchMandatedUserBySCVID() {
        return new SearchMandatedUserBySCVID();
    }

    /**
     * Create an instance of {@link SearchMandatedUserBySCVIDResponse }
     * 
     */
    public SearchMandatedUserBySCVIDResponse createSearchMandatedUserBySCVIDResponse() {
        return new SearchMandatedUserBySCVIDResponse();
    }

    /**
     * Create an instance of {@link SearchMandatedUserBySCVIDFault }
     * 
     */
    public SearchMandatedUserBySCVIDFault createSearchMandatedUserBySCVIDFault() {
        return new SearchMandatedUserBySCVIDFault();
    }

    /**
     * Create an instance of {@link SearchMatchCustByCIF }
     * 
     */
    public SearchMatchCustByCIF createSearchMatchCustByCIF() {
        return new SearchMatchCustByCIF();
    }

    /**
     * Create an instance of {@link SearchMatchCustByCIFResponse }
     * 
     */
    public SearchMatchCustByCIFResponse createSearchMatchCustByCIFResponse() {
        return new SearchMatchCustByCIFResponse();
    }

    /**
     * Create an instance of {@link SearchMatchCustByCIFFault }
     * 
     */
    public SearchMatchCustByCIFFault createSearchMatchCustByCIFFault() {
        return new SearchMatchCustByCIFFault();
    }

    /**
     * Create an instance of {@link SearchMatchCustByMType }
     * 
     */
    public SearchMatchCustByMType createSearchMatchCustByMType() {
        return new SearchMatchCustByMType();
    }

    /**
     * Create an instance of {@link SearchMatchCustByMTypeResponse }
     * 
     */
    public SearchMatchCustByMTypeResponse createSearchMatchCustByMTypeResponse() {
        return new SearchMatchCustByMTypeResponse();
    }

    /**
     * Create an instance of {@link SearchMatchCustByMTypeFault }
     * 
     */
    public SearchMatchCustByMTypeFault createSearchMatchCustByMTypeFault() {
        return new SearchMatchCustByMTypeFault();
    }

    /**
     * Create an instance of {@link SearchNonIndividualCustomer }
     * 
     */
    public SearchNonIndividualCustomer createSearchNonIndividualCustomer() {
        return new SearchNonIndividualCustomer();
    }

    /**
     * Create an instance of {@link SearchNonIndividualCustomerResponse }
     * 
     */
    public SearchNonIndividualCustomerResponse createSearchNonIndividualCustomerResponse() {
        return new SearchNonIndividualCustomerResponse();
    }

    /**
     * Create an instance of {@link SearchNonIndividualCustomerFault }
     * 
     */
    public SearchNonIndividualCustomerFault createSearchNonIndividualCustomerFault() {
        return new SearchNonIndividualCustomerFault();
    }

    /**
     * Create an instance of {@link UnlinkCustFault }
     * 
     */
    public UnlinkCustFault createUnlinkCustFault() {
        return new UnlinkCustFault();
    }

    /**
     * Create an instance of {@link UnlinkCustomer }
     * 
     */
    public UnlinkCustomer createUnlinkCustomer() {
        return new UnlinkCustomer();
    }

    /**
     * Create an instance of {@link UnlinkCustomerResponse }
     * 
     */
    public UnlinkCustomerResponse createUnlinkCustomerResponse() {
        return new UnlinkCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateMandatedUser }
     * 
     */
    public UpdateMandatedUser createUpdateMandatedUser() {
        return new UpdateMandatedUser();
    }

    /**
     * Create an instance of {@link UpdateMandatedUserResponse }
     * 
     */
    public UpdateMandatedUserResponse createUpdateMandatedUserResponse() {
        return new UpdateMandatedUserResponse();
    }

    /**
     * Create an instance of {@link UpdateMandatedUserStatus }
     * 
     */
    public UpdateMandatedUserStatus createUpdateMandatedUserStatus() {
        return new UpdateMandatedUserStatus();
    }

    /**
     * Create an instance of {@link UpdateMandatedUserStatusResponse }
     * 
     */
    public UpdateMandatedUserStatusResponse createUpdateMandatedUserStatusResponse() {
        return new UpdateMandatedUserStatusResponse();
    }

    /**
     * Create an instance of {@link UpdateMandatedUserStatusFault }
     * 
     */
    public UpdateMandatedUserStatusFault createUpdateMandatedUserStatusFault() {
        return new UpdateMandatedUserStatusFault();
    }

    /**
     * Create an instance of {@link UpdateMandatedUserFault }
     * 
     */
    public UpdateMandatedUserFault createUpdateMandatedUserFault() {
        return new UpdateMandatedUserFault();
    }

    /**
     * Create an instance of {@link ValidateCustMobileNumber }
     * 
     */
    public ValidateCustMobileNumber createValidateCustMobileNumber() {
        return new ValidateCustMobileNumber();
    }

    /**
     * Create an instance of {@link ValidateCustMobileNumberResponse }
     * 
     */
    public ValidateCustMobileNumberResponse createValidateCustMobileNumberResponse() {
        return new ValidateCustMobileNumberResponse();
    }

    /**
     * Create an instance of {@link ValidateCustMobileNumberFault }
     * 
     */
    public ValidateCustMobileNumberFault createValidateCustMobileNumberFault() {
        return new ValidateCustMobileNumberFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerScvID }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateCustomerScvID }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "CreateCustomerScvID")
    public JAXBElement<CreateCustomerScvID> createCreateCustomerScvID(CreateCustomerScvID value) {
        return new JAXBElement<CreateCustomerScvID>(_CreateCustomerScvID_QNAME, CreateCustomerScvID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerScvIDResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateCustomerScvIDResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "CreateCustomerScvIDResponse")
    public JAXBElement<CreateCustomerScvIDResponse> createCreateCustomerScvIDResponse(CreateCustomerScvIDResponse value) {
        return new JAXBElement<CreateCustomerScvIDResponse>(_CreateCustomerScvIDResponse_QNAME, CreateCustomerScvIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMandatedUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateMandatedUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "CreateMandatedUser")
    public JAXBElement<CreateMandatedUser> createCreateMandatedUser(CreateMandatedUser value) {
        return new JAXBElement<CreateMandatedUser>(_CreateMandatedUser_QNAME, CreateMandatedUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMandatedUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateMandatedUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "CreateMandatedUserResponse")
    public JAXBElement<CreateMandatedUserResponse> createCreateMandatedUserResponse(CreateMandatedUserResponse value) {
        return new JAXBElement<CreateMandatedUserResponse>(_CreateMandatedUserResponse_QNAME, CreateMandatedUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkCustomer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LinkCustomer }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "LinkCustomer")
    public JAXBElement<LinkCustomer> createLinkCustomer(LinkCustomer value) {
        return new JAXBElement<LinkCustomer>(_LinkCustomer_QNAME, LinkCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkCustomerAuth }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LinkCustomerAuth }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "LinkCustomerAuth")
    public JAXBElement<LinkCustomerAuth> createLinkCustomerAuth(LinkCustomerAuth value) {
        return new JAXBElement<LinkCustomerAuth>(_LinkCustomerAuth_QNAME, LinkCustomerAuth.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkCustomerAuthResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LinkCustomerAuthResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "LinkCustomerAuthResponse")
    public JAXBElement<LinkCustomerAuthResponse> createLinkCustomerAuthResponse(LinkCustomerAuthResponse value) {
        return new JAXBElement<LinkCustomerAuthResponse>(_LinkCustomerAuthResponse_QNAME, LinkCustomerAuthResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkCustomerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LinkCustomerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "LinkCustomerResponse")
    public JAXBElement<LinkCustomerResponse> createLinkCustomerResponse(LinkCustomerResponse value) {
        return new JAXBElement<LinkCustomerResponse>(_LinkCustomerResponse_QNAME, LinkCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveLinkedCustomer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveLinkedCustomer }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "RetrieveLinkedCustomer")
    public JAXBElement<RetrieveLinkedCustomer> createRetrieveLinkedCustomer(RetrieveLinkedCustomer value) {
        return new JAXBElement<RetrieveLinkedCustomer>(_RetrieveLinkedCustomer_QNAME, RetrieveLinkedCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveLinkedCustomerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveLinkedCustomerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "RetrieveLinkedCustomerResponse")
    public JAXBElement<RetrieveLinkedCustomerResponse> createRetrieveLinkedCustomerResponse(RetrieveLinkedCustomerResponse value) {
        return new JAXBElement<RetrieveLinkedCustomerResponse>(_RetrieveLinkedCustomerResponse_QNAME, RetrieveLinkedCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveMandatedUserList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveMandatedUserList }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "RetrieveMandatedUserList")
    public JAXBElement<RetrieveMandatedUserList> createRetrieveMandatedUserList(RetrieveMandatedUserList value) {
        return new JAXBElement<RetrieveMandatedUserList>(_RetrieveMandatedUserList_QNAME, RetrieveMandatedUserList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveMandatedUserListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveMandatedUserListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "RetrieveMandatedUserListResponse")
    public JAXBElement<RetrieveMandatedUserListResponse> createRetrieveMandatedUserListResponse(RetrieveMandatedUserListResponse value) {
        return new JAXBElement<RetrieveMandatedUserListResponse>(_RetrieveMandatedUserListResponse_QNAME, RetrieveMandatedUserListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchAllPendingMatch }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchAllPendingMatch }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchAllPendingMatch")
    public JAXBElement<SearchAllPendingMatch> createSearchAllPendingMatch(SearchAllPendingMatch value) {
        return new JAXBElement<SearchAllPendingMatch>(_SearchAllPendingMatch_QNAME, SearchAllPendingMatch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchAllPendingMatchResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchAllPendingMatchResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchAllPendingMatchResponse")
    public JAXBElement<SearchAllPendingMatchResponse> createSearchAllPendingMatchResponse(SearchAllPendingMatchResponse value) {
        return new JAXBElement<SearchAllPendingMatchResponse>(_SearchAllPendingMatchResponse_QNAME, SearchAllPendingMatchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomer }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomer")
    public JAXBElement<SearchCustomer> createSearchCustomer(SearchCustomer value) {
        return new JAXBElement<SearchCustomer>(_SearchCustomer_QNAME, SearchCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerByIdentifiers }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerByIdentifiers }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerByIdentifiers")
    public JAXBElement<SearchCustomerByIdentifiers> createSearchCustomerByIdentifiers(SearchCustomerByIdentifiers value) {
        return new JAXBElement<SearchCustomerByIdentifiers>(_SearchCustomerByIdentifiers_QNAME, SearchCustomerByIdentifiers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerByIdentifiersResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerByIdentifiersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerByIdentifiersResponse")
    public JAXBElement<SearchCustomerByIdentifiersResponse> createSearchCustomerByIdentifiersResponse(SearchCustomerByIdentifiersResponse value) {
        return new JAXBElement<SearchCustomerByIdentifiersResponse>(_SearchCustomerByIdentifiersResponse_QNAME, SearchCustomerByIdentifiersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerByName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerByName }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerByName")
    public JAXBElement<SearchCustomerByName> createSearchCustomerByName(SearchCustomerByName value) {
        return new JAXBElement<SearchCustomerByName>(_SearchCustomerByName_QNAME, SearchCustomerByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerByNameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerByNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerByNameResponse")
    public JAXBElement<SearchCustomerByNameResponse> createSearchCustomerByNameResponse(SearchCustomerByNameResponse value) {
        return new JAXBElement<SearchCustomerByNameResponse>(_SearchCustomerByNameResponse_QNAME, SearchCustomerByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerByPPId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerByPPId }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerByPPId")
    public JAXBElement<SearchCustomerByPPId> createSearchCustomerByPPId(SearchCustomerByPPId value) {
        return new JAXBElement<SearchCustomerByPPId>(_SearchCustomerByPPId_QNAME, SearchCustomerByPPId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerByPPIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerByPPIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerByPPIdResponse")
    public JAXBElement<SearchCustomerByPPIdResponse> createSearchCustomerByPPIdResponse(SearchCustomerByPPIdResponse value) {
        return new JAXBElement<SearchCustomerByPPIdResponse>(_SearchCustomerByPPIdResponse_QNAME, SearchCustomerByPPIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerBySCVID }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerBySCVID }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerBySCVID")
    public JAXBElement<SearchCustomerBySCVID> createSearchCustomerBySCVID(SearchCustomerBySCVID value) {
        return new JAXBElement<SearchCustomerBySCVID>(_SearchCustomerBySCVID_QNAME, SearchCustomerBySCVID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerBySCVIDResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerBySCVIDResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerBySCVIDResponse")
    public JAXBElement<SearchCustomerBySCVIDResponse> createSearchCustomerBySCVIDResponse(SearchCustomerBySCVIDResponse value) {
        return new JAXBElement<SearchCustomerBySCVIDResponse>(_SearchCustomerBySCVIDResponse_QNAME, SearchCustomerBySCVIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCustomerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchCustomerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchCustomerResponse")
    public JAXBElement<SearchCustomerResponse> createSearchCustomerResponse(SearchCustomerResponse value) {
        return new JAXBElement<SearchCustomerResponse>(_SearchCustomerResponse_QNAME, SearchCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchMandatedUserBySCVID }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchMandatedUserBySCVID }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchMandatedUserBySCVID")
    public JAXBElement<SearchMandatedUserBySCVID> createSearchMandatedUserBySCVID(SearchMandatedUserBySCVID value) {
        return new JAXBElement<SearchMandatedUserBySCVID>(_SearchMandatedUserBySCVID_QNAME, SearchMandatedUserBySCVID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchMandatedUserBySCVIDResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchMandatedUserBySCVIDResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchMandatedUserBySCVIDResponse")
    public JAXBElement<SearchMandatedUserBySCVIDResponse> createSearchMandatedUserBySCVIDResponse(SearchMandatedUserBySCVIDResponse value) {
        return new JAXBElement<SearchMandatedUserBySCVIDResponse>(_SearchMandatedUserBySCVIDResponse_QNAME, SearchMandatedUserBySCVIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByCIF }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByCIF }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchMatchCustByCIF")
    public JAXBElement<SearchMatchCustByCIF> createSearchMatchCustByCIF(SearchMatchCustByCIF value) {
        return new JAXBElement<SearchMatchCustByCIF>(_SearchMatchCustByCIF_QNAME, SearchMatchCustByCIF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByCIFResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByCIFResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchMatchCustByCIFResponse")
    public JAXBElement<SearchMatchCustByCIFResponse> createSearchMatchCustByCIFResponse(SearchMatchCustByCIFResponse value) {
        return new JAXBElement<SearchMatchCustByCIFResponse>(_SearchMatchCustByCIFResponse_QNAME, SearchMatchCustByCIFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByMType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByMType }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchMatchCustByMType")
    public JAXBElement<SearchMatchCustByMType> createSearchMatchCustByMType(SearchMatchCustByMType value) {
        return new JAXBElement<SearchMatchCustByMType>(_SearchMatchCustByMType_QNAME, SearchMatchCustByMType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByMTypeResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchMatchCustByMTypeResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchMatchCustByMTypeResponse")
    public JAXBElement<SearchMatchCustByMTypeResponse> createSearchMatchCustByMTypeResponse(SearchMatchCustByMTypeResponse value) {
        return new JAXBElement<SearchMatchCustByMTypeResponse>(_SearchMatchCustByMTypeResponse_QNAME, SearchMatchCustByMTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchNonIndividualCustomer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchNonIndividualCustomer }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchNonIndividualCustomer")
    public JAXBElement<SearchNonIndividualCustomer> createSearchNonIndividualCustomer(SearchNonIndividualCustomer value) {
        return new JAXBElement<SearchNonIndividualCustomer>(_SearchNonIndividualCustomer_QNAME, SearchNonIndividualCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchNonIndividualCustomerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchNonIndividualCustomerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "SearchNonIndividualCustomerResponse")
    public JAXBElement<SearchNonIndividualCustomerResponse> createSearchNonIndividualCustomerResponse(SearchNonIndividualCustomerResponse value) {
        return new JAXBElement<SearchNonIndividualCustomerResponse>(_SearchNonIndividualCustomerResponse_QNAME, SearchNonIndividualCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlinkCustomer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlinkCustomer }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "UnlinkCustomer")
    public JAXBElement<UnlinkCustomer> createUnlinkCustomer(UnlinkCustomer value) {
        return new JAXBElement<UnlinkCustomer>(_UnlinkCustomer_QNAME, UnlinkCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlinkCustomerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlinkCustomerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "UnlinkCustomerResponse")
    public JAXBElement<UnlinkCustomerResponse> createUnlinkCustomerResponse(UnlinkCustomerResponse value) {
        return new JAXBElement<UnlinkCustomerResponse>(_UnlinkCustomerResponse_QNAME, UnlinkCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "UpdateMandatedUser")
    public JAXBElement<UpdateMandatedUser> createUpdateMandatedUser(UpdateMandatedUser value) {
        return new JAXBElement<UpdateMandatedUser>(_UpdateMandatedUser_QNAME, UpdateMandatedUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "UpdateMandatedUserResponse")
    public JAXBElement<UpdateMandatedUserResponse> createUpdateMandatedUserResponse(UpdateMandatedUserResponse value) {
        return new JAXBElement<UpdateMandatedUserResponse>(_UpdateMandatedUserResponse_QNAME, UpdateMandatedUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUserStatus }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUserStatus }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "UpdateMandatedUserStatus")
    public JAXBElement<UpdateMandatedUserStatus> createUpdateMandatedUserStatus(UpdateMandatedUserStatus value) {
        return new JAXBElement<UpdateMandatedUserStatus>(_UpdateMandatedUserStatus_QNAME, UpdateMandatedUserStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUserStatusResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateMandatedUserStatusResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "UpdateMandatedUserStatusResponse")
    public JAXBElement<UpdateMandatedUserStatusResponse> createUpdateMandatedUserStatusResponse(UpdateMandatedUserStatusResponse value) {
        return new JAXBElement<UpdateMandatedUserStatusResponse>(_UpdateMandatedUserStatusResponse_QNAME, UpdateMandatedUserStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCustMobileNumber }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ValidateCustMobileNumber }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "ValidateCustMobileNumber")
    public JAXBElement<ValidateCustMobileNumber> createValidateCustMobileNumber(ValidateCustMobileNumber value) {
        return new JAXBElement<ValidateCustMobileNumber>(_ValidateCustMobileNumber_QNAME, ValidateCustMobileNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCustMobileNumberResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ValidateCustMobileNumberResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://scv.mce.barclays.com/scv/", name = "ValidateCustMobileNumberResponse")
    public JAXBElement<ValidateCustMobileNumberResponse> createValidateCustMobileNumberResponse(ValidateCustMobileNumberResponse value) {
        return new JAXBElement<ValidateCustMobileNumberResponse>(_ValidateCustMobileNumberResponse_QNAME, ValidateCustMobileNumberResponse.class, null, value);
    }

}
