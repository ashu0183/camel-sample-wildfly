
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchcustbyidentifiersreq.SearchCustByIdentifiersReq;


/**
 * <p>Java class for SearchCustomerByIdentifiers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustomerByIdentifiers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustByIdentifiersReq" type="{http://scv.mce.barclays.com/scv/SearchCustByIdentifiersReq}SearchCustByIdentifiersReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustomerByIdentifiers", propOrder = {
    "searchCustByIdentifiersReq"
})
public class SearchCustomerByIdentifiers {

    @XmlElement(name = "SearchCustByIdentifiersReq")
    protected SearchCustByIdentifiersReq searchCustByIdentifiersReq;

    /**
     * Gets the value of the searchCustByIdentifiersReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCustByIdentifiersReq }
     *     
     */
    public SearchCustByIdentifiersReq getSearchCustByIdentifiersReq() {
        return searchCustByIdentifiersReq;
    }

    /**
     * Sets the value of the searchCustByIdentifiersReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCustByIdentifiersReq }
     *     
     */
    public void setSearchCustByIdentifiersReq(SearchCustByIdentifiersReq value) {
        this.searchCustByIdentifiersReq = value;
    }

}
