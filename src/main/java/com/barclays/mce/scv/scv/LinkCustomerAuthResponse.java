
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for LinkCustomerAuthResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinkCustomerAuthResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LinkCustomerAuthResponse" type="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinkCustomerAuthResponse", propOrder = {
    "linkCustomerAuthResponse"
})
public class LinkCustomerAuthResponse {

    @XmlElement(name = "LinkCustomerAuthResponse")
    protected SCVBaseResponse linkCustomerAuthResponse;

    /**
     * Gets the value of the linkCustomerAuthResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SCVBaseResponse }
     *     
     */
    public SCVBaseResponse getLinkCustomerAuthResponse() {
        return linkCustomerAuthResponse;
    }

    /**
     * Sets the value of the linkCustomerAuthResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SCVBaseResponse }
     *     
     */
    public void setLinkCustomerAuthResponse(SCVBaseResponse value) {
        this.linkCustomerAuthResponse = value;
    }

}
