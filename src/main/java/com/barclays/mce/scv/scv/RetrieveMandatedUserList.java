
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.retrievemandateduserlistreq.RetrieveMandatedUserListReq;


/**
 * <p>Java class for RetrieveMandatedUserList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveMandatedUserList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetrieveMandatedUserListReq" type="{http://scv.mce.barclays.com/scv/RetrieveMandatedUserListReq}RetrieveMandatedUserListReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveMandatedUserList", propOrder = {
    "retrieveMandatedUserListReq"
})
public class RetrieveMandatedUserList {

    @XmlElement(name = "RetrieveMandatedUserListReq")
    protected RetrieveMandatedUserListReq retrieveMandatedUserListReq;

    /**
     * Gets the value of the retrieveMandatedUserListReq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveMandatedUserListReq }
     *     
     */
    public RetrieveMandatedUserListReq getRetrieveMandatedUserListReq() {
        return retrieveMandatedUserListReq;
    }

    /**
     * Sets the value of the retrieveMandatedUserListReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveMandatedUserListReq }
     *     
     */
    public void setRetrieveMandatedUserListReq(RetrieveMandatedUserListReq value) {
        this.retrieveMandatedUserListReq = value;
    }

}
