
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.searchcustomerreq.SearchCustomerReq;


/**
 * <p>Java class for SearchCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchCustomerReq" type="{http://scv.mce.barclays.com/scv/SearchCustomerReq}SearchCustomerReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustomer", propOrder = {
    "searchCustomerReq"
})
public class SearchCustomer {

    @XmlElement(name = "SearchCustomerReq")
    protected SearchCustomerReq searchCustomerReq;

    /**
     * Gets the value of the searchCustomerReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchCustomerReq }
     *     
     */
    public SearchCustomerReq getSearchCustomerReq() {
        return searchCustomerReq;
    }

    /**
     * Sets the value of the searchCustomerReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCustomerReq }
     *     
     */
    public void setSearchCustomerReq(SearchCustomerReq value) {
        this.searchCustomerReq = value;
    }

}
