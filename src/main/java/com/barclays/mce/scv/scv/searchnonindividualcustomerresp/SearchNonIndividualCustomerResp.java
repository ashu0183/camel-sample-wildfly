
package com.barclays.mce.scv.scv.searchnonindividualcustomerresp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.nonindividualcustomerdetails.NonIndividualCustomerDetails;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;
import com.barclays.mce.service.pageinfo.mcepageinfo.MCEPageInfo;


/**
 * <p>Java class for SearchNonIndividualCustomerResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchNonIndividualCustomerResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PageInfo" type="{http://pageinfo.service.mce.barclays.com/MCEPageInfo}MCEPageInfo" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="NonIndividualCustomerDetails" type="{http://scv.mce.barclays.com/scv/NonIndividualCustomerDetails}NonIndividualCustomerDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchNonIndividualCustomerResp", propOrder = {
    "pageInfo",
    "nonIndividualCustomerDetails"
})
public class SearchNonIndividualCustomerResp
    extends SCVBaseResponse
{

    @XmlElement(name = "PageInfo")
    protected MCEPageInfo pageInfo;
    @XmlElement(name = "NonIndividualCustomerDetails")
    protected List<NonIndividualCustomerDetails> nonIndividualCustomerDetails;

    /**
     * Gets the value of the pageInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MCEPageInfo }
     *     
     */
    public MCEPageInfo getPageInfo() {
        return pageInfo;
    }

    /**
     * Sets the value of the pageInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MCEPageInfo }
     *     
     */
    public void setPageInfo(MCEPageInfo value) {
        this.pageInfo = value;
    }

    /**
     * Gets the value of the nonIndividualCustomerDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nonIndividualCustomerDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNonIndividualCustomerDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NonIndividualCustomerDetails }
     * 
     * 
     */
    public List<NonIndividualCustomerDetails> getNonIndividualCustomerDetails() {
        if (nonIndividualCustomerDetails == null) {
            nonIndividualCustomerDetails = new ArrayList<NonIndividualCustomerDetails>();
        }
        return this.nonIndividualCustomerDetails;
    }

}
