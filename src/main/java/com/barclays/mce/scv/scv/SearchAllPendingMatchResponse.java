
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.searchpendingmatchresult.SearchPendingMatchResult;


/**
 * <p>Java class for SearchAllPendingMatchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchAllPendingMatchResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchAllPendingMatchResponse" type="{http://scv.mce.barclays.com/match/SearchPendingMatchResult}SearchPendingMatchResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchAllPendingMatchResponse", propOrder = {
    "searchAllPendingMatchResponse"
})
public class SearchAllPendingMatchResponse {

    @XmlElement(name = "SearchAllPendingMatchResponse")
    protected SearchPendingMatchResult searchAllPendingMatchResponse;

    /**
     * Gets the value of the searchAllPendingMatchResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SearchPendingMatchResult }
     *     
     */
    public SearchPendingMatchResult getSearchAllPendingMatchResponse() {
        return searchAllPendingMatchResponse;
    }

    /**
     * Sets the value of the searchAllPendingMatchResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchPendingMatchResult }
     *     
     */
    public void setSearchAllPendingMatchResponse(SearchPendingMatchResult value) {
        this.searchAllPendingMatchResponse = value;
    }

}
