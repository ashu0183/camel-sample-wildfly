
package com.barclays.mce.scv.scv.validatecustmobilenumberresp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for ValidateCustMobileNumberResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateCustMobileNumberResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MatchFound" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateCustMobileNumberResp", propOrder = {
    "matchFound"
})
public class ValidateCustMobileNumberResp
    extends SCVBaseResponse
{

    @XmlElement(name = "MatchFound")
    protected Boolean matchFound;

    /**
     * Gets the value of the matchFound property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchFound() {
        return matchFound;
    }

    /**
     * Sets the value of the matchFound property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchFound(Boolean value) {
        this.matchFound = value;
    }

}
