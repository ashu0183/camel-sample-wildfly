
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.createmandateduserresp.CreateMandatedUserResp;


/**
 * <p>Java class for CreateMandatedUserResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateMandatedUserResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateMandatedUserResp" type="{http://scv.mce.barclays.com/scv/CreateMandatedUserResp}CreateMandatedUserResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateMandatedUserResponse", propOrder = {
    "createMandatedUserResp"
})
public class CreateMandatedUserResponse {

    @XmlElement(name = "CreateMandatedUserResp")
    protected CreateMandatedUserResp createMandatedUserResp;

    /**
     * Gets the value of the createMandatedUserResp property.
     * 
     * @return
     *     possible object is
     *     {@link CreateMandatedUserResp }
     *     
     */
    public CreateMandatedUserResp getCreateMandatedUserResp() {
        return createMandatedUserResp;
    }

    /**
     * Sets the value of the createMandatedUserResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateMandatedUserResp }
     *     
     */
    public void setCreateMandatedUserResp(CreateMandatedUserResp value) {
        this.createMandatedUserResp = value;
    }

}
