
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.match.searchmatchcustbycifreq.SearchMatchCustByCIFReq;


/**
 * <p>Java class for SearchMatchCustByCIF complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMatchCustByCIF"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchMatchCustByCIFReq" type="{http://scv.mce.barclays.com/match/SearchMatchCustByCIFReq}SearchMatchCustByCIFReq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMatchCustByCIF", propOrder = {
    "searchMatchCustByCIFReq"
})
public class SearchMatchCustByCIF {

    @XmlElement(name = "SearchMatchCustByCIFReq")
    protected SearchMatchCustByCIFReq searchMatchCustByCIFReq;

    /**
     * Gets the value of the searchMatchCustByCIFReq property.
     * 
     * @return
     *     possible object is
     *     {@link SearchMatchCustByCIFReq }
     *     
     */
    public SearchMatchCustByCIFReq getSearchMatchCustByCIFReq() {
        return searchMatchCustByCIFReq;
    }

    /**
     * Sets the value of the searchMatchCustByCIFReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMatchCustByCIFReq }
     *     
     */
    public void setSearchMatchCustByCIFReq(SearchMatchCustByCIFReq value) {
        this.searchMatchCustByCIFReq = value;
    }

}
