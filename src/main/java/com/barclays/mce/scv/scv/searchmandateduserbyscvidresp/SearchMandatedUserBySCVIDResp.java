
package com.barclays.mce.scv.scv.searchmandateduserbyscvidresp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.mandateduser.MandatedUser;
import com.barclays.mce.scv.scv.scvbaseresponse.SCVBaseResponse;


/**
 * <p>Java class for SearchMandatedUserBySCVIDResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchMandatedUserBySCVIDResp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseResponse}SCVBaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MandatedUser" type="{http://scv.mce.barclays.com/scv/MandatedUser}MandatedUser" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchMandatedUserBySCVIDResp", propOrder = {
    "mandatedUser"
})
public class SearchMandatedUserBySCVIDResp
    extends SCVBaseResponse
{

    @XmlElement(name = "MandatedUser")
    protected MandatedUser mandatedUser;

    /**
     * Gets the value of the mandatedUser property.
     * 
     * @return
     *     possible object is
     *     {@link MandatedUser }
     *     
     */
    public MandatedUser getMandatedUser() {
        return mandatedUser;
    }

    /**
     * Sets the value of the mandatedUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link MandatedUser }
     *     
     */
    public void setMandatedUser(MandatedUser value) {
        this.mandatedUser = value;
    }

}
