
package com.barclays.mce.scv.scv.searchnonindividualcustomerreq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaserequest.SCVBaseRequest;
import com.barclays.mce.service.pageinfo.mcepageinfo.MCEPageInfo;


/**
 * <p>Java class for SearchNonIndividualCustomerReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchNonIndividualCustomerReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseRequest}SCVBaseRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="SCVID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="BankCIF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="RegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="PageInfo" type="{http://pageinfo.service.mce.barclays.com/MCEPageInfo}MCEPageInfo" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchNonIndividualCustomerReq", propOrder = {
    "searchType",
    "scvid",
    "bankCIF",
    "companyName",
    "registrationNumber",
    "pageInfo"
})
public class SearchNonIndividualCustomerReq
    extends SCVBaseRequest
{

    @XmlElement(name = "SearchType")
    protected String searchType;
    @XmlElement(name = "SCVID")
    protected String scvid;
    @XmlElement(name = "BankCIF")
    protected String bankCIF;
    @XmlElement(name = "CompanyName")
    protected String companyName;
    @XmlElement(name = "RegistrationNumber")
    protected String registrationNumber;
    @XmlElement(name = "PageInfo")
    protected MCEPageInfo pageInfo;

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchType(String value) {
        this.searchType = value;
    }

    /**
     * Gets the value of the scvid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCVID() {
        return scvid;
    }

    /**
     * Sets the value of the scvid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCVID(String value) {
        this.scvid = value;
    }

    /**
     * Gets the value of the bankCIF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCIF() {
        return bankCIF;
    }

    /**
     * Sets the value of the bankCIF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCIF(String value) {
        this.bankCIF = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the registrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * Sets the value of the registrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationNumber(String value) {
        this.registrationNumber = value;
    }

    /**
     * Gets the value of the pageInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MCEPageInfo }
     *     
     */
    public MCEPageInfo getPageInfo() {
        return pageInfo;
    }

    /**
     * Sets the value of the pageInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MCEPageInfo }
     *     
     */
    public void setPageInfo(MCEPageInfo value) {
        this.pageInfo = value;
    }

}
