
package com.barclays.mce.scv.scv.scverrorlist;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scverror.SCVError;


/**
 * <p>Java class for SCVErrorList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SCVErrorList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SCVError" type="{http://scv.mce.barclays.com/scv/SCVError}SCVError" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCVErrorList", propOrder = {
    "scvError"
})
public class SCVErrorList {

    @XmlElement(name = "SCVError")
    protected List<SCVError> scvError;

    /**
     * Gets the value of the scvError property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scvError property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCVError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCVError }
     * 
     * 
     */
    public List<SCVError> getSCVError() {
        if (scvError == null) {
            scvError = new ArrayList<SCVError>();
        }
        return this.scvError;
    }

}
