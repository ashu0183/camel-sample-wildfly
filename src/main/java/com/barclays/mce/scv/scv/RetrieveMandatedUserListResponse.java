
package com.barclays.mce.scv.scv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.retrievemandateduserlistresp.RetrieveMandatedUserListResp;


/**
 * <p>Java class for RetrieveMandatedUserListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveMandatedUserListResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetrieveMandatedUserListResp" type="{http://scv.mce.barclays.com/scv/RetrieveMandatedUserListResp}RetrieveMandatedUserListResp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveMandatedUserListResponse", propOrder = {
    "retrieveMandatedUserListResp"
})
public class RetrieveMandatedUserListResponse {

    @XmlElement(name = "RetrieveMandatedUserListResp")
    protected RetrieveMandatedUserListResp retrieveMandatedUserListResp;

    /**
     * Gets the value of the retrieveMandatedUserListResp property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveMandatedUserListResp }
     *     
     */
    public RetrieveMandatedUserListResp getRetrieveMandatedUserListResp() {
        return retrieveMandatedUserListResp;
    }

    /**
     * Sets the value of the retrieveMandatedUserListResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveMandatedUserListResp }
     *     
     */
    public void setRetrieveMandatedUserListResp(RetrieveMandatedUserListResp value) {
        this.retrieveMandatedUserListResp = value;
    }

}
