
package com.barclays.mce.scv.scv.searchcustbyscvidreq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.barclays.mce.scv.scv.scvbaserequest.SCVBaseRequest;


/**
 * <p>Java class for SearchCustBySCVIDReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCustBySCVIDReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://scv.mce.barclays.com/scv/SCVBaseRequest}SCVBaseRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SCVID" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCustBySCVIDReq", propOrder = {
    "scvid"
})
public class SearchCustBySCVIDReq
    extends SCVBaseRequest
{

    @XmlElement(name = "SCVID", required = true)
    protected String scvid;

    /**
     * Gets the value of the scvid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCVID() {
        return scvid;
    }

    /**
     * Sets the value of the scvid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCVID(String value) {
        this.scvid = value;
    }

}
