
package com.barclays.grcb.mce.userauditdetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserAuditDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserAuditDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Createdby" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="LastMaintenanceActionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="LastMaintainedByUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="LastMaintainedDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="AuthorizedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="AuthorizedDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="AuthorizationStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAuditDetails", propOrder = {
    "createdby",
    "createDateTime",
    "lastMaintenanceActionCode",
    "lastMaintainedByUserID",
    "lastMaintainedDateTime",
    "authorizedBy",
    "authorizedDateTime",
    "authorizationStatusCode"
})
public class UserAuditDetails {

    @XmlElement(name = "Createdby")
    protected String createdby;
    @XmlElement(name = "CreateDateTime")
    protected String createDateTime;
    @XmlElement(name = "LastMaintenanceActionCode")
    protected String lastMaintenanceActionCode;
    @XmlElement(name = "LastMaintainedByUserID")
    protected String lastMaintainedByUserID;
    @XmlElement(name = "LastMaintainedDateTime")
    protected String lastMaintainedDateTime;
    @XmlElement(name = "AuthorizedBy")
    protected String authorizedBy;
    @XmlElement(name = "AuthorizedDateTime")
    protected String authorizedDateTime;
    @XmlElement(name = "AuthorizationStatusCode")
    protected String authorizationStatusCode;

    /**
     * Gets the value of the createdby property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedby() {
        return createdby;
    }

    /**
     * Sets the value of the createdby property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedby(String value) {
        this.createdby = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateDateTime(String value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the lastMaintenanceActionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastMaintenanceActionCode() {
        return lastMaintenanceActionCode;
    }

    /**
     * Sets the value of the lastMaintenanceActionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastMaintenanceActionCode(String value) {
        this.lastMaintenanceActionCode = value;
    }

    /**
     * Gets the value of the lastMaintainedByUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastMaintainedByUserID() {
        return lastMaintainedByUserID;
    }

    /**
     * Sets the value of the lastMaintainedByUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastMaintainedByUserID(String value) {
        this.lastMaintainedByUserID = value;
    }

    /**
     * Gets the value of the lastMaintainedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastMaintainedDateTime() {
        return lastMaintainedDateTime;
    }

    /**
     * Sets the value of the lastMaintainedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastMaintainedDateTime(String value) {
        this.lastMaintainedDateTime = value;
    }

    /**
     * Gets the value of the authorizedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedBy() {
        return authorizedBy;
    }

    /**
     * Sets the value of the authorizedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedBy(String value) {
        this.authorizedBy = value;
    }

    /**
     * Gets the value of the authorizedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedDateTime() {
        return authorizedDateTime;
    }

    /**
     * Sets the value of the authorizedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedDateTime(String value) {
        this.authorizedDateTime = value;
    }

    /**
     * Gets the value of the authorizationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationStatusCode() {
        return authorizationStatusCode;
    }

    /**
     * Sets the value of the authorizationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationStatusCode(String value) {
        this.authorizationStatusCode = value;
    }

}
